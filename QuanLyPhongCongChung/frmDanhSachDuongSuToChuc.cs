﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
namespace QuanLyPhongCongChung
{
    public partial class frmDanhSachDuongSuToChuc : DevExpress.XtraEditors.XtraForm
    {
        public frmDanhSachDuongSuToChuc()
        {
            InitializeComponent();
        }

        private void frmDanhSachDuongSu_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            var b = new DataPhongCongChungDataContext();
            gcData.DataSource = b.sp_getDanhSachDuongSu().Where(a=>a.LoaiDoiTuong=="TOCHUC").ToList();
        }
        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            deleteData();
        }

        public void deleteData() {
            try
            {
                if (Utilities.ShowConfirmMessage("Bạn chắc chắn muốn xóa?"))
                {
                    int row_index = gvData.FocusedRowHandle;
                    string field_name = "Id";
                    object value = gvData.GetRowCellValue(row_index, field_name);
                    if (value != null)
                    {
                        using (var db = new DataPhongCongChungDataContext())
                        {
                            Guid id = new Guid(value.ToString());
                            var _DuongSu = db.DuongSus.Single(a => a.Id == id);
                            if (_DuongSu != null)
                            {
                                _DuongSu.IsDeleted = true;
                                db.SubmitChanges();
                                Utilities.ShowMessageSuccess("Đã xóa thành công");
                                LoadData();
                            }
                        }
                        
                    }
                    else
                    {
                        Utilities.ShowMessageError("Chưa chọn đương sự để xóa");
                    }
                }
            }
            catch (Exception)
            {
                Utilities.ShowMessageError("Lỗi");
            }
        }

        public void UpdateDuongSu()
        {
            try
            {
                frmUpdateDuongSu _UpdateDuongSu = new frmUpdateDuongSu();
                _UpdateDuongSu.is_insert = false;
                _UpdateDuongSu.loaiduongsu = "TOCHUC";
                int row_index = gvData.FocusedRowHandle;
                string field_name = "Id";
                object value = gvData.GetRowCellValue(row_index, field_name);
                if (value != null)
                {
                    _UpdateDuongSu.id_DuongSu = new Guid(value.ToString());
                    _UpdateDuongSu.ShowDialog();
                }
                LoadData();
            }
            catch (Exception)
            {
                Utilities.ShowMessageError("Lỗi");
            }
        }
        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmUpdateDuongSu themDuongSu = new frmUpdateDuongSu();
            themDuongSu.is_insert = true;
            themDuongSu.loaiduongsu = "TOCHUC";
            DialogResult dialogResult;
            dialogResult = themDuongSu.ShowDialog();
            LoadData();

        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UpdateDuongSu();
        }

        private void frmDanhSachDuongSu_KeyDown(object sender, KeyEventArgs e)
        {
            frmUpdateDuongSu updateDuongSu = new frmUpdateDuongSu();
            if (e.KeyCode == Keys.F4)
            {
                btnThem_ItemClick(null, null);
            }
            if (e.KeyCode == Keys.F3)
            {
                UpdateDuongSu();
            }
            if (e.KeyCode == Keys.F8)
            {
                deleteData();
            }
            if (e.KeyCode == Keys.F9)
            {
                btnXuatExcel_ItemClick(null, null);
            }
        }

        private void btnXuatExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string folderName = "";
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath;
                string FileName = folderName+"\\DanhSachDuongSuToChuc.xls";
                gvData.ExportToXls(FileName, Utilities.printingOptionFormat());
            }
            
        }
    }
}
