﻿namespace QuanLyPhongCongChung
{
    partial class frmUpdateDuongSu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdateDuongSu));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnLuu = new DevExpress.XtraBars.BarButtonItem();
            this.btnDong = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.xtDuongSu = new DevExpress.XtraTab.XtraTabControl();
            this.xtpCaNhan = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtSoGiayToTuyThan = new DevExpress.XtraEditors.TextEdit();
            this.txtSoBaoTu = new DevExpress.XtraEditors.TextEdit();
            this.txtNoiCapGiayChungTu = new DevExpress.XtraEditors.TextEdit();
            this.txtNoiMat = new DevExpress.XtraEditors.TextEdit();
            this.txtDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.txtQuocTich = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.txtHoKhauThuongTru = new DevExpress.XtraEditors.TextEdit();
            this.txtMST = new DevExpress.XtraEditors.TextEdit();
            this.txtNoiCap = new DevExpress.XtraEditors.TextEdit();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.txtNgayCap = new DevExpress.XtraEditors.DateEdit();
            this.txtNgaySinh = new DevExpress.XtraEditors.TextEdit();
            this.txtNgayMat = new DevExpress.XtraEditors.DateEdit();
            this.txtNgayCapGiayChungTu = new DevExpress.XtraEditors.DateEdit();
            this.txtGioiTinh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtTinhTrangHonNhan = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtTinhTrangSongChet = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtLoaiGiayToTuyThan = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcTinhTrang = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtpToChuc = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.txtChucVu = new DevExpress.XtraEditors.TextEdit();
            this.txtHoTenNguoiDaiDien = new DevExpress.XtraEditors.TextEdit();
            this.txtGiayPhepKinhDoanh = new DevExpress.XtraEditors.TextEdit();
            this.txtDienThoai_tc = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi_tc = new DevExpress.XtraEditors.TextEdit();
            this.txtMST_tc = new DevExpress.XtraEditors.TextEdit();
            this.txtTenToChuc = new DevExpress.XtraEditors.TextEdit();
            this.txtXungHo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtNgayCap_tc = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtDuongSu)).BeginInit();
            this.xtDuongSu.SuspendLayout();
            this.xtpCaNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiayToTuyThan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoBaoTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiCapGiayChungTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiMat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuocTich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoKhauThuongTru.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMST.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayMat.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayMat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCapGiayChungTu.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCapGiayChungTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhTrangHonNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhTrangSongChet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoaiGiayToTuyThan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcTinhTrang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.xtpToChuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTenNguoiDaiDien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiayPhepKinhDoanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai_tc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi_tc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMST_tc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenToChuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXungHo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap_tc.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap_tc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLuu,
            this.btnDong});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLuu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDong, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnLuu
            // 
            this.btnLuu.Caption = "Lưu (Ctrl + S)";
            this.btnLuu.Glyph = global::QuanLyPhongCongChung.Properties.Resources.save;
            this.btnLuu.Id = 0;
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLuu_ItemClick);
            // 
            // btnDong
            // 
            this.btnDong.Caption = "Đóng (Esc)";
            this.btnDong.Glyph = global::QuanLyPhongCongChung.Properties.Resources._1491322890_f_cross_256;
            this.btnDong.Id = 1;
            this.btnDong.Name = "btnDong";
            this.btnDong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(929, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 379);
            this.barDockControlBottom.Size = new System.Drawing.Size(929, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 347);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(929, 32);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 347);
            // 
            // xtDuongSu
            // 
            this.xtDuongSu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtDuongSu.Location = new System.Drawing.Point(0, 32);
            this.xtDuongSu.Name = "xtDuongSu";
            this.xtDuongSu.SelectedTabPage = this.xtpCaNhan;
            this.xtDuongSu.Size = new System.Drawing.Size(929, 347);
            this.xtDuongSu.TabIndex = 4;
            this.xtDuongSu.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtpCaNhan,
            this.xtpToChuc});
            this.xtDuongSu.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtDuongSu_SelectedPageChanged);
            // 
            // xtpCaNhan
            // 
            this.xtpCaNhan.Controls.Add(this.layoutControl1);
            this.xtpCaNhan.Name = "xtpCaNhan";
            this.xtpCaNhan.Size = new System.Drawing.Size(923, 319);
            this.xtpCaNhan.Text = "Cá nhân";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtSoGiayToTuyThan);
            this.layoutControl1.Controls.Add(this.txtSoBaoTu);
            this.layoutControl1.Controls.Add(this.txtNoiCapGiayChungTu);
            this.layoutControl1.Controls.Add(this.txtNoiMat);
            this.layoutControl1.Controls.Add(this.txtDienThoai);
            this.layoutControl1.Controls.Add(this.txtQuocTich);
            this.layoutControl1.Controls.Add(this.txtDiaChi);
            this.layoutControl1.Controls.Add(this.txtHoKhauThuongTru);
            this.layoutControl1.Controls.Add(this.txtMST);
            this.layoutControl1.Controls.Add(this.txtNoiCap);
            this.layoutControl1.Controls.Add(this.txtHoTen);
            this.layoutControl1.Controls.Add(this.txtNgayCap);
            this.layoutControl1.Controls.Add(this.txtNgaySinh);
            this.layoutControl1.Controls.Add(this.txtNgayMat);
            this.layoutControl1.Controls.Add(this.txtNgayCapGiayChungTu);
            this.layoutControl1.Controls.Add(this.txtGioiTinh);
            this.layoutControl1.Controls.Add(this.txtTinhTrangHonNhan);
            this.layoutControl1.Controls.Add(this.txtTinhTrangSongChet);
            this.layoutControl1.Controls.Add(this.txtLoaiGiayToTuyThan);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(411, 73, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(923, 319);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtSoGiayToTuyThan
            // 
            this.txtSoGiayToTuyThan.Location = new System.Drawing.Point(582, 36);
            this.txtSoGiayToTuyThan.MenuManager = this.barManager1;
            this.txtSoGiayToTuyThan.Name = "txtSoGiayToTuyThan";
            this.txtSoGiayToTuyThan.Size = new System.Drawing.Size(329, 20);
            this.txtSoGiayToTuyThan.StyleController = this.layoutControl1;
            this.txtSoGiayToTuyThan.TabIndex = 28;
            // 
            // txtSoBaoTu
            // 
            this.txtSoBaoTu.Location = new System.Drawing.Point(130, 276);
            this.txtSoBaoTu.MenuManager = this.barManager1;
            this.txtSoBaoTu.Name = "txtSoBaoTu";
            this.txtSoBaoTu.Size = new System.Drawing.Size(330, 20);
            this.txtSoBaoTu.StyleController = this.layoutControl1;
            this.txtSoBaoTu.TabIndex = 27;
            // 
            // txtNoiCapGiayChungTu
            // 
            this.txtNoiCapGiayChungTu.Location = new System.Drawing.Point(130, 228);
            this.txtNoiCapGiayChungTu.MenuManager = this.barManager1;
            this.txtNoiCapGiayChungTu.Name = "txtNoiCapGiayChungTu";
            this.txtNoiCapGiayChungTu.Size = new System.Drawing.Size(330, 20);
            this.txtNoiCapGiayChungTu.StyleController = this.layoutControl1;
            this.txtNoiCapGiayChungTu.TabIndex = 25;
            // 
            // txtNoiMat
            // 
            this.txtNoiMat.Location = new System.Drawing.Point(130, 204);
            this.txtNoiMat.MenuManager = this.barManager1;
            this.txtNoiMat.Name = "txtNoiMat";
            this.txtNoiMat.Size = new System.Drawing.Size(330, 20);
            this.txtNoiMat.StyleController = this.layoutControl1;
            this.txtNoiMat.TabIndex = 24;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Location = new System.Drawing.Point(130, 84);
            this.txtDienThoai.MenuManager = this.barManager1;
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(330, 20);
            this.txtDienThoai.StyleController = this.layoutControl1;
            this.txtDienThoai.TabIndex = 20;
            // 
            // txtQuocTich
            // 
            this.txtQuocTich.EditValue = "Việt Nam";
            this.txtQuocTich.Location = new System.Drawing.Point(582, 84);
            this.txtQuocTich.MenuManager = this.barManager1;
            this.txtQuocTich.Name = "txtQuocTich";
            this.txtQuocTich.Size = new System.Drawing.Size(329, 20);
            this.txtQuocTich.StyleController = this.layoutControl1;
            this.txtQuocTich.TabIndex = 19;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(582, 108);
            this.txtDiaChi.MenuManager = this.barManager1;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(329, 20);
            this.txtDiaChi.StyleController = this.layoutControl1;
            this.txtDiaChi.TabIndex = 18;
            // 
            // txtHoKhauThuongTru
            // 
            this.txtHoKhauThuongTru.Location = new System.Drawing.Point(130, 108);
            this.txtHoKhauThuongTru.MenuManager = this.barManager1;
            this.txtHoKhauThuongTru.Name = "txtHoKhauThuongTru";
            this.txtHoKhauThuongTru.Size = new System.Drawing.Size(330, 20);
            this.txtHoKhauThuongTru.StyleController = this.layoutControl1;
            this.txtHoKhauThuongTru.TabIndex = 17;
            // 
            // txtMST
            // 
            this.txtMST.Location = new System.Drawing.Point(130, 132);
            this.txtMST.MenuManager = this.barManager1;
            this.txtMST.Name = "txtMST";
            this.txtMST.Size = new System.Drawing.Size(330, 20);
            this.txtMST.StyleController = this.layoutControl1;
            this.txtMST.TabIndex = 16;
            // 
            // txtNoiCap
            // 
            this.txtNoiCap.Location = new System.Drawing.Point(582, 60);
            this.txtNoiCap.MenuManager = this.barManager1;
            this.txtNoiCap.Name = "txtNoiCap";
            this.txtNoiCap.Size = new System.Drawing.Size(329, 20);
            this.txtNoiCap.StyleController = this.layoutControl1;
            this.txtNoiCap.TabIndex = 15;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(130, 12);
            this.txtHoTen.MenuManager = this.barManager1;
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(330, 20);
            this.txtHoTen.StyleController = this.layoutControl1;
            this.txtHoTen.TabIndex = 9;
            // 
            // txtNgayCap
            // 
            this.txtNgayCap.EditValue = null;
            this.txtNgayCap.Location = new System.Drawing.Point(130, 60);
            this.txtNgayCap.MenuManager = this.barManager1;
            this.txtNgayCap.Name = "txtNgayCap";
            this.txtNgayCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayCap.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayCap.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayCap.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayCap.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayCap.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayCap.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.txtNgayCap.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtNgayCap.Size = new System.Drawing.Size(330, 20);
            this.txtNgayCap.StyleController = this.layoutControl1;
            this.txtNgayCap.TabIndex = 14;
            // 
            // txtNgaySinh
            // 
            this.txtNgaySinh.Location = new System.Drawing.Point(807, 12);
            this.txtNgaySinh.MenuManager = this.barManager1;
            this.txtNgaySinh.Name = "txtNgaySinh";
            this.txtNgaySinh.Size = new System.Drawing.Size(104, 20);
            this.txtNgaySinh.StyleController = this.layoutControl1;
            this.txtNgaySinh.TabIndex = 11;
            // 
            // txtNgayMat
            // 
            this.txtNgayMat.EditValue = null;
            this.txtNgayMat.Location = new System.Drawing.Point(130, 180);
            this.txtNgayMat.MenuManager = this.barManager1;
            this.txtNgayMat.Name = "txtNgayMat";
            this.txtNgayMat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayMat.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayMat.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayMat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayMat.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayMat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayMat.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.txtNgayMat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtNgayMat.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtNgayMat.Size = new System.Drawing.Size(330, 20);
            this.txtNgayMat.StyleController = this.layoutControl1;
            this.txtNgayMat.TabIndex = 23;
            // 
            // txtNgayCapGiayChungTu
            // 
            this.txtNgayCapGiayChungTu.EditValue = null;
            this.txtNgayCapGiayChungTu.Location = new System.Drawing.Point(130, 252);
            this.txtNgayCapGiayChungTu.MenuManager = this.barManager1;
            this.txtNgayCapGiayChungTu.Name = "txtNgayCapGiayChungTu";
            this.txtNgayCapGiayChungTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayCapGiayChungTu.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayCapGiayChungTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayCapGiayChungTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayCapGiayChungTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayCapGiayChungTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayCapGiayChungTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.txtNgayCapGiayChungTu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtNgayCapGiayChungTu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtNgayCapGiayChungTu.Size = new System.Drawing.Size(330, 20);
            this.txtNgayCapGiayChungTu.StyleController = this.layoutControl1;
            this.txtNgayCapGiayChungTu.TabIndex = 26;
            // 
            // txtGioiTinh
            // 
            this.txtGioiTinh.EditValue = "";
            this.txtGioiTinh.Location = new System.Drawing.Point(582, 12);
            this.txtGioiTinh.MenuManager = this.barManager1;
            this.txtGioiTinh.Name = "txtGioiTinh";
            this.txtGioiTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtGioiTinh.Properties.Items.AddRange(new object[] {
            "Nam",
            "Nữ",
            "Khác"});
            this.txtGioiTinh.Properties.PopupSizeable = true;
            this.txtGioiTinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtGioiTinh.Size = new System.Drawing.Size(103, 20);
            this.txtGioiTinh.StyleController = this.layoutControl1;
            this.txtGioiTinh.TabIndex = 10;
            // 
            // txtTinhTrangHonNhan
            // 
            this.txtTinhTrangHonNhan.Location = new System.Drawing.Point(582, 132);
            this.txtTinhTrangHonNhan.MenuManager = this.barManager1;
            this.txtTinhTrangHonNhan.Name = "txtTinhTrangHonNhan";
            this.txtTinhTrangHonNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTinhTrangHonNhan.Properties.Items.AddRange(new object[] {
            "",
            "Độc thân",
            "Đã kết hôn",
            "Đã li hôn"});
            this.txtTinhTrangHonNhan.Properties.PopupSizeable = true;
            this.txtTinhTrangHonNhan.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtTinhTrangHonNhan.Size = new System.Drawing.Size(329, 20);
            this.txtTinhTrangHonNhan.StyleController = this.layoutControl1;
            this.txtTinhTrangHonNhan.TabIndex = 21;
            // 
            // txtTinhTrangSongChet
            // 
            this.txtTinhTrangSongChet.Location = new System.Drawing.Point(130, 156);
            this.txtTinhTrangSongChet.MenuManager = this.barManager1;
            this.txtTinhTrangSongChet.Name = "txtTinhTrangSongChet";
            this.txtTinhTrangSongChet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTinhTrangSongChet.Properties.Items.AddRange(new object[] {
            "Sống",
            "Chết"});
            this.txtTinhTrangSongChet.Properties.PopupSizeable = true;
            this.txtTinhTrangSongChet.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtTinhTrangSongChet.Size = new System.Drawing.Size(330, 20);
            this.txtTinhTrangSongChet.StyleController = this.layoutControl1;
            this.txtTinhTrangSongChet.TabIndex = 22;
            this.txtTinhTrangSongChet.SelectedIndexChanged += new System.EventHandler(this.txtTinhTrangSongChet_SelectedIndexChanged);
            // 
            // txtLoaiGiayToTuyThan
            // 
            this.txtLoaiGiayToTuyThan.EditValue = "";
            this.txtLoaiGiayToTuyThan.Location = new System.Drawing.Point(130, 36);
            this.txtLoaiGiayToTuyThan.MenuManager = this.barManager1;
            this.txtLoaiGiayToTuyThan.Name = "txtLoaiGiayToTuyThan";
            this.txtLoaiGiayToTuyThan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtLoaiGiayToTuyThan.Properties.NullText = "";
            this.txtLoaiGiayToTuyThan.Size = new System.Drawing.Size(330, 20);
            this.txtLoaiGiayToTuyThan.StyleController = this.layoutControl1;
            this.txtLoaiGiayToTuyThan.TabIndex = 12;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem19,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem13,
            this.layoutControlItem8,
            this.lcTinhTrang});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(923, 319);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtHoTen;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem1.Text = "Họ tên *";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtHoKhauThuongTru;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem9.Text = "Thường trú tại";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtDiaChi;
            this.layoutControlItem10.Location = new System.Drawing.Point(452, 96);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem10.Text = "Nơi ở hiện tại";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtTinhTrangHonNhan;
            this.layoutControlItem12.Location = new System.Drawing.Point(452, 120);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(451, 179);
            this.layoutControlItem12.Text = "Tình trạng hôn nhân";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtQuocTich;
            this.layoutControlItem5.Location = new System.Drawing.Point(452, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem5.Text = "Quốc tịch";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtDienThoai;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem11.Text = "Điện thoại";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtGioiTinh;
            this.layoutControlItem2.Location = new System.Drawing.Point(452, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem2.Text = "Giới tính *";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtNgaySinh;
            this.layoutControlItem3.Location = new System.Drawing.Point(677, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem3.Text = "Ngày sinh";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtLoaiGiayToTuyThan;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem4.Text = "Loại giấy tờ tùy thân *";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtSoGiayToTuyThan;
            this.layoutControlItem19.Location = new System.Drawing.Point(452, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem19.Text = "Số giấy tờ tùy thân *";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtNgayCap;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem6.Text = "Ngày cấp";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtNoiCap;
            this.layoutControlItem7.Location = new System.Drawing.Point(452, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem7.Text = "Nơi cấp";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtTinhTrangSongChet;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem13.Text = "Tình trạng đương sự";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtMST;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem8.Text = "Mã số thuế";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(115, 13);
            // 
            // lcTinhTrang
            // 
            this.lcTinhTrang.GroupBordersVisible = false;
            this.lcTinhTrang.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem18,
            this.layoutControlItem16,
            this.layoutControlItem15,
            this.layoutControlItem17});
            this.lcTinhTrang.Location = new System.Drawing.Point(0, 168);
            this.lcTinhTrang.Name = "lcTinhTrang";
            this.lcTinhTrang.Size = new System.Drawing.Size(452, 131);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtNgayMat;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem14.Text = "Ngày mất";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txtSoBaoTu;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(452, 35);
            this.layoutControlItem18.Text = "Sổ báo tử";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtNoiCapGiayChungTu;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem16.Text = "Nơi cấp giấy chứng tử";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtNoiMat;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem15.Text = "Nơi mất";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtNgayCapGiayChungTu;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem17.Text = "Ngày cấp giấy chứng tử";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(115, 13);
            // 
            // xtpToChuc
            // 
            this.xtpToChuc.Controls.Add(this.layoutControl2);
            this.xtpToChuc.Name = "xtpToChuc";
            this.xtpToChuc.Size = new System.Drawing.Size(923, 319);
            this.xtpToChuc.Text = "Tổ chức";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.txtChucVu);
            this.layoutControl2.Controls.Add(this.txtHoTenNguoiDaiDien);
            this.layoutControl2.Controls.Add(this.txtGiayPhepKinhDoanh);
            this.layoutControl2.Controls.Add(this.txtDienThoai_tc);
            this.layoutControl2.Controls.Add(this.txtDiaChi_tc);
            this.layoutControl2.Controls.Add(this.txtMST_tc);
            this.layoutControl2.Controls.Add(this.txtTenToChuc);
            this.layoutControl2.Controls.Add(this.txtXungHo);
            this.layoutControl2.Controls.Add(this.txtNgayCap_tc);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(923, 319);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // txtChucVu
            // 
            this.txtChucVu.Location = new System.Drawing.Point(132, 132);
            this.txtChucVu.MenuManager = this.barManager1;
            this.txtChucVu.Name = "txtChucVu";
            this.txtChucVu.Size = new System.Drawing.Size(328, 20);
            this.txtChucVu.StyleController = this.layoutControl2;
            this.txtChucVu.TabIndex = 11;
            // 
            // txtHoTenNguoiDaiDien
            // 
            this.txtHoTenNguoiDaiDien.Location = new System.Drawing.Point(132, 108);
            this.txtHoTenNguoiDaiDien.MenuManager = this.barManager1;
            this.txtHoTenNguoiDaiDien.Name = "txtHoTenNguoiDaiDien";
            this.txtHoTenNguoiDaiDien.Size = new System.Drawing.Size(328, 20);
            this.txtHoTenNguoiDaiDien.StyleController = this.layoutControl2;
            this.txtHoTenNguoiDaiDien.TabIndex = 9;
            // 
            // txtGiayPhepKinhDoanh
            // 
            this.txtGiayPhepKinhDoanh.Location = new System.Drawing.Point(132, 84);
            this.txtGiayPhepKinhDoanh.MenuManager = this.barManager1;
            this.txtGiayPhepKinhDoanh.Name = "txtGiayPhepKinhDoanh";
            this.txtGiayPhepKinhDoanh.Size = new System.Drawing.Size(328, 20);
            this.txtGiayPhepKinhDoanh.StyleController = this.layoutControl2;
            this.txtGiayPhepKinhDoanh.TabIndex = 8;
            // 
            // txtDienThoai_tc
            // 
            this.txtDienThoai_tc.Location = new System.Drawing.Point(132, 60);
            this.txtDienThoai_tc.MenuManager = this.barManager1;
            this.txtDienThoai_tc.Name = "txtDienThoai_tc";
            this.txtDienThoai_tc.Size = new System.Drawing.Size(328, 20);
            this.txtDienThoai_tc.StyleController = this.layoutControl2;
            this.txtDienThoai_tc.TabIndex = 7;
            // 
            // txtDiaChi_tc
            // 
            this.txtDiaChi_tc.Location = new System.Drawing.Point(132, 36);
            this.txtDiaChi_tc.MenuManager = this.barManager1;
            this.txtDiaChi_tc.Name = "txtDiaChi_tc";
            this.txtDiaChi_tc.Size = new System.Drawing.Size(779, 20);
            this.txtDiaChi_tc.StyleController = this.layoutControl2;
            this.txtDiaChi_tc.TabIndex = 6;
            // 
            // txtMST_tc
            // 
            this.txtMST_tc.Location = new System.Drawing.Point(584, 60);
            this.txtMST_tc.MenuManager = this.barManager1;
            this.txtMST_tc.Name = "txtMST_tc";
            this.txtMST_tc.Size = new System.Drawing.Size(327, 20);
            this.txtMST_tc.StyleController = this.layoutControl2;
            this.txtMST_tc.TabIndex = 5;
            // 
            // txtTenToChuc
            // 
            this.txtTenToChuc.Location = new System.Drawing.Point(132, 12);
            this.txtTenToChuc.MenuManager = this.barManager1;
            this.txtTenToChuc.Name = "txtTenToChuc";
            this.txtTenToChuc.Size = new System.Drawing.Size(779, 20);
            this.txtTenToChuc.StyleController = this.layoutControl2;
            this.txtTenToChuc.TabIndex = 4;
            // 
            // txtXungHo
            // 
            this.txtXungHo.EditValue = "";
            this.txtXungHo.Location = new System.Drawing.Point(584, 108);
            this.txtXungHo.MenuManager = this.barManager1;
            this.txtXungHo.Name = "txtXungHo";
            this.txtXungHo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtXungHo.Properties.Items.AddRange(new object[] {
            "Ông",
            "Bà"});
            this.txtXungHo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtXungHo.Size = new System.Drawing.Size(327, 20);
            this.txtXungHo.StyleController = this.layoutControl2;
            this.txtXungHo.TabIndex = 10;
            // 
            // txtNgayCap_tc
            // 
            this.txtNgayCap_tc.EditValue = null;
            this.txtNgayCap_tc.Location = new System.Drawing.Point(584, 84);
            this.txtNgayCap_tc.MenuManager = this.barManager1;
            this.txtNgayCap_tc.Name = "txtNgayCap_tc";
            this.txtNgayCap_tc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayCap_tc.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgayCap_tc.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayCap_tc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayCap_tc.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtNgayCap_tc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgayCap_tc.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.txtNgayCap_tc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtNgayCap_tc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtNgayCap_tc.Size = new System.Drawing.Size(327, 20);
            this.txtNgayCap_tc.StyleController = this.layoutControl2;
            this.txtNgayCap_tc.TabIndex = 12;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem25,
            this.layoutControlItem23,
            this.layoutControlItem28,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem24,
            this.layoutControlItem21});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(923, 319);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtTenToChuc;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(903, 24);
            this.layoutControlItem20.Text = "Tên tổ chức *";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.txtDiaChi_tc;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(903, 24);
            this.layoutControlItem22.Text = "Địa chỉ";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.txtHoTenNguoiDaiDien;
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem25.Text = "Họ tên người đại diện";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txtDienThoai_tc;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem23.Text = "Điện thoại";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txtNgayCap_tc;
            this.layoutControlItem28.Location = new System.Drawing.Point(452, 72);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem28.Text = "Ngày cấp";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.txtXungHo;
            this.layoutControlItem26.Location = new System.Drawing.Point(452, 96);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(451, 203);
            this.layoutControlItem26.Text = "Xưng hô";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txtChucVu;
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(452, 179);
            this.layoutControlItem27.Text = "Chức vụ";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txtGiayPhepKinhDoanh;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(452, 24);
            this.layoutControlItem24.Text = "Giấy phép kinh doanh số";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtMST_tc;
            this.layoutControlItem21.Location = new System.Drawing.Point(452, 48);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem21.Text = "Mã số thuế *";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(117, 13);
            // 
            // frmUpdateDuongSu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 379);
            this.Controls.Add(this.xtDuongSu);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmUpdateDuongSu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cập nhật đương sự";
            this.Load += new System.EventHandler(this.frmDuongSuUpdate_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmDuongSuUpdate_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtDuongSu)).EndInit();
            this.xtDuongSu.ResumeLayout(false);
            this.xtpCaNhan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiayToTuyThan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoBaoTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiCapGiayChungTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiMat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuocTich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoKhauThuongTru.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMST.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayMat.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayMat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCapGiayChungTu.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCapGiayChungTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhTrangHonNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhTrangSongChet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoaiGiayToTuyThan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcTinhTrang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.xtpToChuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTenNguoiDaiDien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiayPhepKinhDoanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai_tc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi_tc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMST_tc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenToChuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXungHo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap_tc.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayCap_tc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraTab.XtraTabControl xtDuongSu;
        private DevExpress.XtraTab.XtraTabPage xtpCaNhan;
        private DevExpress.XtraTab.XtraTabPage xtpToChuc;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.TextEdit txtHoKhauThuongTru;
        private DevExpress.XtraEditors.TextEdit txtMST;
        private DevExpress.XtraEditors.TextEdit txtNoiCap;
        private DevExpress.XtraEditors.TextEdit txtHoTen;
        private DevExpress.XtraEditors.DateEdit txtNgayCap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtNgaySinh;
        private DevExpress.XtraEditors.TextEdit txtQuocTich;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtDienThoai;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit txtSoBaoTu;
        private DevExpress.XtraEditors.TextEdit txtNoiCapGiayChungTu;
        private DevExpress.XtraEditors.TextEdit txtNoiMat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.DateEdit txtNgayMat;
        private DevExpress.XtraEditors.DateEdit txtNgayCapGiayChungTu;
        private DevExpress.XtraBars.BarButtonItem btnLuu;
        private DevExpress.XtraBars.BarButtonItem btnDong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtSoGiayToTuyThan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.ComboBoxEdit txtGioiTinh;
        private DevExpress.XtraEditors.ComboBoxEdit txtTinhTrangHonNhan;
        private DevExpress.XtraEditors.ComboBoxEdit txtTinhTrangSongChet;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit txtHoTenNguoiDaiDien;
        private DevExpress.XtraEditors.TextEdit txtGiayPhepKinhDoanh;
        private DevExpress.XtraEditors.TextEdit txtDienThoai_tc;
        private DevExpress.XtraEditors.TextEdit txtDiaChi_tc;
        private DevExpress.XtraEditors.TextEdit txtMST_tc;
        private DevExpress.XtraEditors.TextEdit txtTenToChuc;
        private DevExpress.XtraEditors.ComboBoxEdit txtXungHo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.TextEdit txtChucVu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.DateEdit txtNgayCap_tc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlGroup lcTinhTrang;
        private DevExpress.XtraEditors.LookUpEdit txtLoaiGiayToTuyThan;
    }
}