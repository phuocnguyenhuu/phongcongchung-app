﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyPhongCongChung
{
    public partial class frmCapNhatLoaiHoSo : DevExpress.XtraEditors.XtraForm
    {
        public frmCapNhatLoaiHoSo()
        {
            InitializeComponent();
        }

        public Guid Id_LoaiHoSo;
        public bool is_insert = true;

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            save();
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void frmCapNhatLoaiHoSo_Load(object sender, EventArgs e)
        {
            fillDataUpdate();
        }
        private void fillDataUpdate()
        {
            if (!is_insert)
            {
                using (var db = new DataPhongCongChungDataContext())
                {
                    var _LoaiHoSo = db.LoaiHoSos.Where(a => a.Id == Id_LoaiHoSo).SingleOrDefault();
                    if (_LoaiHoSo != null)
                    {
                        txtTen.EditValue = _LoaiHoSo.Ten;
                    }
                }
            }
        }
        private bool save()
        {
                string _Ten = "";
            int count = 0;
            if (txtTen.EditValue != null)
            {
                _Ten = txtTen.EditValue.ToString();
            }
            else
            {
                Utilities.ShowMessageError("Bạn chưa nhập loại hồ sơ");
                txtTen.Focus();
                return false;
            }
            try
            {
                using (var db = new DataPhongCongChungDataContext())
                {
                    count = db.LoaiHoSos.Where(a => a.Ten == txtTen.EditValue.ToString() && a.Id != Id_LoaiHoSo).Where(a=>a.IsDeleled==false).Count();
                    if (count <= 0)
                    {
                        LoaiHoSo _LoaiHoSo = null;
                        if (is_insert)
                        {
                            _LoaiHoSo = new LoaiHoSo();
                            _LoaiHoSo.Id = Guid.NewGuid();
                        }
                        else
                        {
                            _LoaiHoSo = db.LoaiHoSos.Where(a => a.Id == Id_LoaiHoSo).SingleOrDefault();
                        }
                        _LoaiHoSo.Ten = _Ten;

                        if (is_insert)
                        {
                            db.LoaiHoSos.InsertOnSubmit(_LoaiHoSo);
                        }
                        db.SubmitChanges();
                        if (is_insert)
                        {
                            Utilities.ShowMessageSuccess("Lưu thành công");
                        }
                        else
                        {
                            Utilities.ShowMessageSuccess("Sửa thành công");
                        }
                        return true;
                    }
                    else
                    {
                        Utilities.ShowMessageError(" Tài khoản người dùng đã tồn tại");
                        return false;
                    }

                }
            }
            catch (Exception)
            {
                return false;

            }
        }

        private void frmCapNhatLoaiHoSo_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e, this);
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                barButtonItem3_ItemClick(null, null);
            }
        }
    }
}