﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyPhongCongChung
{
    public partial class frmLoaiHoSo : DevExpress.XtraEditors.XtraForm
    {
        public frmLoaiHoSo()
        {
            InitializeComponent();
        }

        private void frmNguoiDung_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void loadData()
        {
            DataPhongCongChungDataContext db = new DataPhongCongChungDataContext();

            var dsLoaiHoSo = db.LoaiHoSos.Select(a => new
            {
                a.Id,
                a.Ten,
                a.IsDeleled
            } 
            ).Where(a => a.IsDeleled == false);
            gcData.DataSource = dsLoaiHoSo.ToList();
            colId.Visible = false;

        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            deleteData();

        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCapNhatLoaiHoSo _CapNhatLoaiHoSo = new frmCapNhatLoaiHoSo();
            _CapNhatLoaiHoSo.ShowDialog();
            loadData();
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCapNhatLoaiHoSo _CapNhatLoaiHoSo = new frmCapNhatLoaiHoSo();
            _CapNhatLoaiHoSo.is_insert = false;
            int row_index = gvData.FocusedRowHandle;
            string field_name = "Id";
            object value = gvData.GetRowCellValue(row_index, field_name);
            if (value != null)
            {
                _CapNhatLoaiHoSo.Id_LoaiHoSo = new Guid(value.ToString());
                _CapNhatLoaiHoSo.ShowDialog();
                loadData();
            }
            else
            {
                Utilities.ShowMessageError("Bạn chưa chọn dòng dữ liệu để sửa");
            }
        }

        private void btnCapNhat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadData();
        }

        private void gcData_Click(object sender, EventArgs e)
        {

        }
        public void deleteData()
        {
            try
            {
                if (Utilities.ShowConfirmMessage("Bạn chắc chắn muốn xóa?"))
                {
                    int row_index = gvData.FocusedRowHandle;
                    string field_name = "Id";
                    object value = gvData.GetRowCellValue(row_index, field_name);
                    if (value != null)
                    {
                        using (var db = new DataPhongCongChungDataContext())
                        {
                            Guid id = new Guid(value.ToString());
                            var _LoaiHoSo = db.LoaiHoSos.Single(a => a.Id == id);
                            if (_LoaiHoSo != null)
                            {
                                _LoaiHoSo.IsDeleled = true;
                                db.SubmitChanges();
                                Utilities.ShowMessageSuccess("Đã xóa thành công");
                                loadData();
                            }
                        }

                    }
                    else
                    {
                        Utilities.ShowMessageError("Chưa chọn hồ sơ để xóa");
                    }
                }
            }
            catch (Exception)
            {
                Utilities.ShowMessageError("Lỗi");
            }
        }


        private void frmLoaiHoSo_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
        }
        private void frmLoaiHoSo_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e, this);
            if (e.KeyCode == Keys.F4)
            {
                btnThem_ItemClick(null, null);
            }
            if (e.KeyCode == Keys.F3)
            {
                btnSua_ItemClick(null, null);
            }
            if (e.KeyCode == Keys.F5)
            {
                btnCapNhat_ItemClick(null, null);
            }
            if (e.KeyCode == Keys.F8)
            {
                btnXoa_ItemClick(null, null);
            }
        }
    }
}