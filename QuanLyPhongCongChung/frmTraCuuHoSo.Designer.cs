﻿namespace QuanLyPhongCongChung
{
    partial class frmTraCuuHoSo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTraCuuHoSo));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnTimKiem = new DevExpress.XtraEditors.SimpleButton();
            this.txtThongTinGhiChu = new DevExpress.XtraEditors.TextEdit();
            this.txtThongTinTaiSan = new DevExpress.XtraEditors.TextEdit();
            this.txtNoiDungHopDong = new DevExpress.XtraEditors.TextEdit();
            this.slDuongSu = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHoTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNamSinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaiGiayTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoGiayTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtSoCongChung = new DevExpress.XtraEditors.TextEdit();
            this.txtTenHoSo = new DevExpress.XtraEditors.TextEdit();
            this.txtCongChungVien = new DevExpress.XtraEditors.TextEdit();
            this.dtTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.dtDenNgay = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gcData = new DevExpress.XtraGrid.GridControl();
            this.gvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSoCongChung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayLap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoTenCongChungVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoiDungHoSo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenLoaiHoSo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThongTinTaiSan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaCongChung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnXuatExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThongTinGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThongTinTaiSan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDungHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slDuongSu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoCongChung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHoSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCongChungVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTuNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDenNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 32);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1327, 173);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnTimKiem);
            this.layoutControl1.Controls.Add(this.txtThongTinGhiChu);
            this.layoutControl1.Controls.Add(this.txtThongTinTaiSan);
            this.layoutControl1.Controls.Add(this.txtNoiDungHopDong);
            this.layoutControl1.Controls.Add(this.slDuongSu);
            this.layoutControl1.Controls.Add(this.txtSoCongChung);
            this.layoutControl1.Controls.Add(this.txtTenHoSo);
            this.layoutControl1.Controls.Add(this.txtCongChungVien);
            this.layoutControl1.Controls.Add(this.dtTuNgay);
            this.layoutControl1.Controls.Add(this.dtDenNgay);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1323, 169);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Appearance.Options.UseFont = true;
            this.btnTimKiem.Location = new System.Drawing.Point(12, 132);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(1299, 22);
            this.btnTimKiem.StyleController = this.layoutControl1;
            this.btnTimKiem.TabIndex = 13;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtThongTinGhiChu
            // 
            this.txtThongTinGhiChu.Location = new System.Drawing.Point(105, 108);
            this.txtThongTinGhiChu.Name = "txtThongTinGhiChu";
            this.txtThongTinGhiChu.Size = new System.Drawing.Size(555, 20);
            this.txtThongTinGhiChu.StyleController = this.layoutControl1;
            this.txtThongTinGhiChu.TabIndex = 12;
            // 
            // txtThongTinTaiSan
            // 
            this.txtThongTinTaiSan.Location = new System.Drawing.Point(757, 84);
            this.txtThongTinTaiSan.Name = "txtThongTinTaiSan";
            this.txtThongTinTaiSan.Size = new System.Drawing.Size(554, 20);
            this.txtThongTinTaiSan.StyleController = this.layoutControl1;
            this.txtThongTinTaiSan.TabIndex = 11;
            // 
            // txtNoiDungHopDong
            // 
            this.txtNoiDungHopDong.Location = new System.Drawing.Point(105, 84);
            this.txtNoiDungHopDong.Name = "txtNoiDungHopDong";
            this.txtNoiDungHopDong.Size = new System.Drawing.Size(555, 20);
            this.txtNoiDungHopDong.StyleController = this.layoutControl1;
            this.txtNoiDungHopDong.TabIndex = 10;
            // 
            // slDuongSu
            // 
            this.slDuongSu.Location = new System.Drawing.Point(105, 60);
            this.slDuongSu.Name = "slDuongSu";
            this.slDuongSu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.slDuongSu.Properties.NullText = "";
            this.slDuongSu.Properties.View = this.searchLookUpEdit1View;
            this.slDuongSu.Size = new System.Drawing.Size(555, 20);
            this.slDuongSu.StyleController = this.layoutControl1;
            this.slDuongSu.TabIndex = 8;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHoTen,
            this.colGioiTinh,
            this.colNamSinh,
            this.colLoaiGiayTo,
            this.colSoGiayTo});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colHoTen
            // 
            this.colHoTen.Caption = "Họ tên";
            this.colHoTen.FieldName = "HoTen";
            this.colHoTen.Name = "colHoTen";
            this.colHoTen.Visible = true;
            this.colHoTen.VisibleIndex = 0;
            // 
            // colGioiTinh
            // 
            this.colGioiTinh.Caption = "Giới tính";
            this.colGioiTinh.FieldName = "GioiTinh";
            this.colGioiTinh.Name = "colGioiTinh";
            this.colGioiTinh.Visible = true;
            this.colGioiTinh.VisibleIndex = 1;
            // 
            // colNamSinh
            // 
            this.colNamSinh.Caption = "Năm sinh";
            this.colNamSinh.FieldName = "NgaySinh";
            this.colNamSinh.Name = "colNamSinh";
            this.colNamSinh.Visible = true;
            this.colNamSinh.VisibleIndex = 2;
            // 
            // colLoaiGiayTo
            // 
            this.colLoaiGiayTo.Caption = "Loại giấy tờ";
            this.colLoaiGiayTo.FieldName = "LoaiGiayToTuyThan";
            this.colLoaiGiayTo.Name = "colLoaiGiayTo";
            this.colLoaiGiayTo.Visible = true;
            this.colLoaiGiayTo.VisibleIndex = 3;
            // 
            // colSoGiayTo
            // 
            this.colSoGiayTo.Caption = "Số giấy tờ";
            this.colSoGiayTo.FieldName = "SoGiayToTuyThan";
            this.colSoGiayTo.Name = "colSoGiayTo";
            this.colSoGiayTo.Visible = true;
            this.colSoGiayTo.VisibleIndex = 4;
            // 
            // txtSoCongChung
            // 
            this.txtSoCongChung.Location = new System.Drawing.Point(757, 36);
            this.txtSoCongChung.Name = "txtSoCongChung";
            this.txtSoCongChung.Size = new System.Drawing.Size(554, 20);
            this.txtSoCongChung.StyleController = this.layoutControl1;
            this.txtSoCongChung.TabIndex = 7;
            // 
            // txtTenHoSo
            // 
            this.txtTenHoSo.Location = new System.Drawing.Point(105, 36);
            this.txtTenHoSo.Name = "txtTenHoSo";
            this.txtTenHoSo.Size = new System.Drawing.Size(555, 20);
            this.txtTenHoSo.StyleController = this.layoutControl1;
            this.txtTenHoSo.TabIndex = 6;
            // 
            // txtCongChungVien
            // 
            this.txtCongChungVien.EditValue = "";
            this.txtCongChungVien.Location = new System.Drawing.Point(757, 60);
            this.txtCongChungVien.Name = "txtCongChungVien";
            this.txtCongChungVien.Properties.NullText = "[EditValue is null]";
            this.txtCongChungVien.Size = new System.Drawing.Size(554, 20);
            this.txtCongChungVien.StyleController = this.layoutControl1;
            this.txtCongChungVien.TabIndex = 9;
            // 
            // dtTuNgay
            // 
            this.dtTuNgay.EditValue = null;
            this.dtTuNgay.Location = new System.Drawing.Point(105, 12);
            this.dtTuNgay.Name = "dtTuNgay";
            this.dtTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTuNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTuNgay.Properties.DisplayFormat.FormatString = "";
            this.dtTuNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtTuNgay.Properties.EditFormat.FormatString = "";
            this.dtTuNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtTuNgay.Properties.Mask.EditMask = "";
            this.dtTuNgay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.dtTuNgay.Size = new System.Drawing.Size(555, 20);
            this.dtTuNgay.StyleController = this.layoutControl1;
            this.dtTuNgay.TabIndex = 4;
            // 
            // dtDenNgay
            // 
            this.dtDenNgay.EditValue = null;
            this.dtDenNgay.Location = new System.Drawing.Point(757, 12);
            this.dtDenNgay.Name = "dtDenNgay";
            this.dtDenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtDenNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtDenNgay.Properties.DisplayFormat.FormatString = "";
            this.dtDenNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtDenNgay.Properties.EditFormat.FormatString = "";
            this.dtDenNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtDenNgay.Properties.Mask.EditMask = "";
            this.dtDenNgay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.dtDenNgay.Size = new System.Drawing.Size(554, 20);
            this.dtDenNgay.StyleController = this.layoutControl1;
            this.dtDenNgay.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1323, 169);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dtTuNgay;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem1.Text = "Từ ngày";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dtDenNgay;
            this.layoutControlItem2.Location = new System.Drawing.Point(652, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(651, 24);
            this.layoutControlItem2.Text = "Đến ngày";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtTenHoSo;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem3.Text = "Tên hồ sơ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtSoCongChung;
            this.layoutControlItem4.Location = new System.Drawing.Point(652, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(651, 24);
            this.layoutControlItem4.Text = "Số công chứng";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.slDuongSu;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem5.Text = "Đương sự";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtCongChungVien;
            this.layoutControlItem6.Location = new System.Drawing.Point(652, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(651, 24);
            this.layoutControlItem6.Text = "Công chứng viên";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtNoiDungHopDong;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem7.Text = "Nội dung hợp đồng";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtThongTinTaiSan;
            this.layoutControlItem8.Location = new System.Drawing.Point(652, 72);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(651, 48);
            this.layoutControlItem8.Text = "Thông tin tài sản";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtThongTinGhiChu;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem9.Text = "Thông tin ghi chú";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnTimKiem;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(1303, 29);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // gcData
            // 
            this.gcData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcData.Location = new System.Drawing.Point(0, 205);
            this.gcData.MainView = this.gvData;
            this.gcData.Name = "gcData";
            this.gcData.Size = new System.Drawing.Size(1327, 324);
            this.gcData.TabIndex = 11;
            this.gcData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvData});
            // 
            // gvData
            // 
            this.gvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSoCongChung,
            this.colTenHopDong,
            this.colNgayLap,
            this.colHoTenCongChungVien,
            this.colNoiDungHoSo,
            this.colTenLoaiHoSo,
            this.colThongTinTaiSan,
            this.colGiaCongChung});
            this.gvData.GridControl = this.gcData;
            this.gvData.Name = "gvData";
            this.gvData.OptionsView.ShowFooter = true;
            // 
            // colSoCongChung
            // 
            this.colSoCongChung.Caption = "Số công chứng";
            this.colSoCongChung.FieldName = "SoCongChung";
            this.colSoCongChung.Name = "colSoCongChung";
            this.colSoCongChung.OptionsColumn.AllowEdit = false;
            this.colSoCongChung.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "SoCongChung", "Số lượng: {0:N0}")});
            this.colSoCongChung.Visible = true;
            this.colSoCongChung.VisibleIndex = 0;
            // 
            // colTenHopDong
            // 
            this.colTenHopDong.Caption = "Tên hợp đồng";
            this.colTenHopDong.FieldName = "TenHopDong";
            this.colTenHopDong.Name = "colTenHopDong";
            this.colTenHopDong.OptionsColumn.AllowEdit = false;
            this.colTenHopDong.Visible = true;
            this.colTenHopDong.VisibleIndex = 1;
            // 
            // colNgayLap
            // 
            this.colNgayLap.Caption = "Ngày Lập";
            this.colNgayLap.FieldName = "NgayLap";
            this.colNgayLap.Name = "colNgayLap";
            this.colNgayLap.OptionsColumn.AllowEdit = false;
            this.colNgayLap.Visible = true;
            this.colNgayLap.VisibleIndex = 2;
            // 
            // colHoTenCongChungVien
            // 
            this.colHoTenCongChungVien.Caption = "Công chứng viên";
            this.colHoTenCongChungVien.FieldName = "HoTenCongChungVien";
            this.colHoTenCongChungVien.Name = "colHoTenCongChungVien";
            this.colHoTenCongChungVien.OptionsColumn.AllowEdit = false;
            this.colHoTenCongChungVien.Visible = true;
            this.colHoTenCongChungVien.VisibleIndex = 3;
            // 
            // colNoiDungHoSo
            // 
            this.colNoiDungHoSo.Caption = "Nội dung hồ sơ";
            this.colNoiDungHoSo.FieldName = "NoiDungHoSo";
            this.colNoiDungHoSo.Name = "colNoiDungHoSo";
            this.colNoiDungHoSo.OptionsColumn.AllowEdit = false;
            this.colNoiDungHoSo.Visible = true;
            this.colNoiDungHoSo.VisibleIndex = 4;
            // 
            // colTenLoaiHoSo
            // 
            this.colTenLoaiHoSo.Caption = "Tên loại hồ sơ";
            this.colTenLoaiHoSo.FieldName = "TenLoaiHoSo";
            this.colTenLoaiHoSo.Name = "colTenLoaiHoSo";
            // 
            // colThongTinTaiSan
            // 
            this.colThongTinTaiSan.Caption = "Thông tin tài sản";
            this.colThongTinTaiSan.FieldName = "ThongTinTaiSan";
            this.colThongTinTaiSan.Name = "colThongTinTaiSan";
            this.colThongTinTaiSan.OptionsColumn.AllowEdit = false;
            this.colThongTinTaiSan.Visible = true;
            this.colThongTinTaiSan.VisibleIndex = 5;
            // 
            // colGiaCongChung
            // 
            this.colGiaCongChung.Caption = "Giá công chứng";
            this.colGiaCongChung.DisplayFormat.FormatString = "N0";
            this.colGiaCongChung.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGiaCongChung.FieldName = "GiaCongChung";
            this.colGiaCongChung.Name = "colGiaCongChung";
            this.colGiaCongChung.OptionsColumn.AllowEdit = false;
            this.colGiaCongChung.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "GiaCongChung", "Tổng cộng: {0:N0}")});
            this.colGiaCongChung.Visible = true;
            this.colGiaCongChung.VisibleIndex = 6;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnXuatExcel});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnXuatExcel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Caption = "Xuất Excel";
            this.btnXuatExcel.Glyph = global::QuanLyPhongCongChung.Properties.Resources.excel;
            this.btnXuatExcel.Id = 0;
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXuatExcel_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1327, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 529);
            this.barDockControlBottom.Size = new System.Drawing.Size(1327, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 497);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1327, 32);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 497);
            // 
            // frmTraCuuHoSo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1327, 529);
            this.Controls.Add(this.gcData);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmTraCuuHoSo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TRA CỨU HỒ SƠ";
            this.Load += new System.EventHandler(this.frmTraCuuHoSo_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmTraCuuHoSo_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtThongTinGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThongTinTaiSan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiDungHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slDuongSu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoCongChung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHoSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCongChungVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTuNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDenNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtSoCongChung;
        private DevExpress.XtraEditors.TextEdit txtTenHoSo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SearchLookUpEdit slDuongSu;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtThongTinGhiChu;
        private DevExpress.XtraEditors.TextEdit txtThongTinTaiSan;
        private DevExpress.XtraEditors.TextEdit txtNoiDungHopDong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.GridControl gcData;
        private DevExpress.XtraGrid.Views.Grid.GridView gvData;
        private DevExpress.XtraGrid.Columns.GridColumn colSoCongChung;
        private DevExpress.XtraGrid.Columns.GridColumn colTenHopDong;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayLap;
        private DevExpress.XtraGrid.Columns.GridColumn colHoTenCongChungVien;
        private DevExpress.XtraGrid.Columns.GridColumn colNoiDungHoSo;
        private DevExpress.XtraGrid.Columns.GridColumn colTenLoaiHoSo;
        private DevExpress.XtraGrid.Columns.GridColumn colThongTinTaiSan;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaCongChung;
        private DevExpress.XtraGrid.Columns.GridColumn colHoTen;
        private DevExpress.XtraGrid.Columns.GridColumn colGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn colNamSinh;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiGiayTo;
        private DevExpress.XtraGrid.Columns.GridColumn colSoGiayTo;
        private DevExpress.XtraEditors.TextEdit txtCongChungVien;
        private DevExpress.XtraEditors.DateEdit dtTuNgay;
        private DevExpress.XtraEditors.DateEdit dtDenNgay;
        private DevExpress.XtraEditors.SimpleButton btnTimKiem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnXuatExcel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}