﻿using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongCongChung
{
    class Utilities
    {
        public static void ShowMessageError(String message)
        {
            XtraMessageBox.Show(message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static void ShowMessageSuccess(String message)
        {
            XtraMessageBox.Show(message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static bool ShowConfirmMessage(string message)
        {
            return XtraMessageBox.Show(message, "Thông báo xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }
        public static void HotKeyDongForm(KeyEventArgs e, Form from)
        {
            if (e.KeyCode == Keys.Escape)
            {
                from.Close();
            }
        }        
        public static void ShowDialogForm(Form frm)
        {
            frm.ShowDialog();
        }
        public static bool IsEmptyData(object input)
        {
            if (input != null)
            {
                if (String.Empty.Equals(input.ToString()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public static string GetStringFromObjectInput(object obj)
        {
            if (obj != null)
            {
                return obj.ToString();
            }
            return String.Empty;
        }

        public static DateTime GetDateTimeFromObjectInput(object obj)
        {
            if (obj != null)
            {
                return Convert.ToDateTime(obj);
            }
            return DateTime.Now;
        }

        public static Guid GetGuidFromObjectInput(object obj)
        {
            if (obj != null)
            {
                return new Guid(obj.ToString());
            }
            return Guid.Empty;
        }
        public static string GetValueFromObjectInput(object obj)
        {
            if (obj != null)
            {
                return obj.ToString();
            }
            return String.Empty;
        }
        public static DateTime GetDenNgay(DateTime date)
        {
            return date.AddDays(1).AddSeconds(-1);
        }

        public static XlsExportOptionsEx printingOptionFormat()
        {
            XlsExportOptionsEx options = new XlsExportOptionsEx();
            options.ShowGridLines = false;
            options.AllowSortingAndFiltering = DevExpress.Utils.DefaultBoolean.False;
            options.ExportType = DevExpress.Export.ExportType.WYSIWYG;
            options.ExportMode = XlsExportMode.SingleFile;
            options.FitToPrintedPageWidth = true;   
            return options;
        }
    }
}
