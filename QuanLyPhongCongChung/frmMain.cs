﻿using DevExpress.XtraTabbedMdi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyPhongCongChung
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
        }
        private Form KiemTraTonTai(Type fType)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.GetType() == fType) // Neu Form duoc truyen vao da duoc mo
                {
                    return f;
                }
            }
            return null;
        }

        private void btDuongSu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmDanhSachDuongSuToChuc));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmDanhSachDuongSuToChuc f = new frmDanhSachDuongSuToChuc();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnDoiMatKhau_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmDoiMatKhau frm = new frmDoiMatKhau();
            frm.ShowDialog();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        private void btnHoSo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmDanhSachHoSo));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmDanhSachHoSo f = new frmDanhSachHoSo();
                f.MdiParent = this;
                f.Show();
            }
            
        }

        private void btnQuanLyNguoiDung_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmNguoiDung));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmNguoiDung f = new frmNguoiDung();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
          
            disableMenu();
            if (LoginInfo.nguoiDungInfo == null)
            {                
                frmLogin frm = new frmLogin();
                frm.ShowDialog();
                disableMenu();
            }
            
        }

        private void disableMenu()
        {
            if (LoginInfo.nguoiDungInfo == null)
            {
                pageGroupHoSo.Enabled = false;
                pageGroupDuongSu.Enabled = false;
                pageGroupNguoiDung.Enabled = false;
                pageGroupPhanMemDieuKhien.Enabled = false;
                pageGroupTroGiup.Enabled = false;
                
            }
            else
            {
                pageGroupHoSo.Enabled = true;
                pageGroupDuongSu.Enabled = true;
                pageGroupNguoiDung.Enabled = true;
                pageGroupPhanMemDieuKhien.Enabled = true;
                pageGroupTroGiup.Enabled = true;
            }
            checkRole();
        }
        private void checkRole()
        {
            if (LoginInfo.nguoiDungInfo != null)
            {
                if (LoginInfo.nguoiDungInfo.IsAdmin)
                {
                    btnQuanLyNguoiDung.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                }
                else
                {
                    btnQuanLyNguoiDung.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                }
            }
            else
            {
                btnQuanLyNguoiDung.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
        }
        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoginInfo.nguoiDungInfo = null;
            disableMenu();           
            frmLogin frm = new frmLogin();
            frm.ShowDialog();
        }

        private void btnTraCuuHoSo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmTraCuuHoSo));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmTraCuuHoSo f = new frmTraCuuHoSo();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmDanhSachDuongSuCaNhan));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmDanhSachDuongSuCaNhan f = new frmDanhSachDuongSuCaNhan();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnCongChung_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCapNhatHoSo f = new frmCapNhatHoSo();
            f.ShowDialog();
        }

        private void btnTeamviwer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            runAppSupport("TeamViewerQS.exe");
        }

        private void btnUltraviewer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {            
            runAppSupport("UltraViewer_setup_6.0_vi.exe");
        }

        private void btnAnyDesk_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {            
            runAppSupport("AnyDesk.exe");
        }

        private void runAppSupport(string appFileName)
        {
            try
            {
                Process process = Process.Start(Application.StartupPath + "\\Soft\\" + appFileName);
                int id = process.Id;
                Process tempProc = Process.GetProcessById(id);
                this.Visible = false;
                tempProc.WaitForExit();
                this.Visible = true;
            }
            catch (Exception)
            {
            }
        }

        private void btnTroGiupKyThuat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmHelp f = new frmHelp();
            f.ShowDialog();
        }

        private void btnDanhMucLoaiHoSo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmLoaiHoSo));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmLoaiHoSo f = new frmLoaiHoSo();
                f.MdiParent = this;
                f.Show();
            }
        }
    }
}
