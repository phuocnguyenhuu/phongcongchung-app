﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongCongChung
{
    public partial class frmCapNhatThongTinDonVi : DevExpress.XtraEditors.XtraForm
    {
        public frmCapNhatThongTinDonVi()
        {
            InitializeComponent();
        }

        private void frmCapNhatThongTinDonVi_Load(object sender, EventArgs e)
        {
            fillData();
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            if (saveData())
            {
                Utilities.ShowMessageSuccess("Cập nhật thành công");
            }
            else
            {
                Utilities.ShowMessageError("Cập nhật thất bại");
            }
        }

        private void fillData()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                var donVi = db.sp_getThongTinDonVi().FirstOrDefault();
                if (donVi != null)
                {
                    txtTenDonVi.EditValue = donVi.TenPhongCC;
                    txtDiaChi.EditValue = donVi.DiaChi;
                    txtDienThoai.EditValue = donVi.DienThoai;
                    txtEmail.EditValue = donVi.Email;
                    txtNguoiDaiDien.EditValue = donVi.NguoiDaiDien;
                    txtId.EditValue = donVi.Id;
                }
            }
        }

        private bool saveData()
        {
            try
            {
                if (validateInput())
                {
                    using (var db = new DataPhongCongChungDataContext())
                    {
                        var donVi = db.ThongTinDonVis.Where(a => a.Id == GetIdDonVi()).SingleOrDefault();
                        if (donVi != null)
                        {
                            donVi.TenPhongCC = Utilities.GetStringFromObjectInput(txtTenDonVi.EditValue);
                            donVi.DiaChi = Utilities.GetStringFromObjectInput(txtDiaChi.EditValue);
                            donVi.DienThoai = Utilities.GetStringFromObjectInput(txtDienThoai.EditValue);
                            donVi.Email = Utilities.GetStringFromObjectInput(txtEmail.EditValue);
                            donVi.NguoiDaiDien = Utilities.GetStringFromObjectInput(txtNguoiDaiDien.EditValue);
                            db.SubmitChanges();
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            
            return false;
        }
        private bool validateInput()
        {
            if (Utilities.IsEmptyData(txtTenDonVi.EditValue))
            {
                Utilities.ShowMessageError("Chưa nhập tên đơn vị");
                txtTenDonVi.Focus();
                return false;
            }
            return true;
        }
        private Guid GetIdDonVi()
        {
            if (txtId.EditValue != null)
            {
                return new Guid(txtId.EditValue.ToString());
            }
            return Guid.Empty;
        }

        private void frmCapNhatThongTinDonVi_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e, this);
            if (e.KeyCode == Keys.Enter)
            {
                btnDongY_Click(null,null);
            }
        }
    }
}
