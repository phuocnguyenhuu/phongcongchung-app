﻿namespace QuanLyPhongCongChung
{
    partial class frmCapNhatHoSo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCapNhatHoSo));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnLuuVaTiepTuc = new DevExpress.XtraBars.BarButtonItem();
            this.btnLuuVaDong = new DevExpress.XtraBars.BarButtonItem();
            this.btnThemDuongSuCaNhan = new DevExpress.XtraBars.BarButtonItem();
            this.btnThemDuongSuToChuc = new DevExpress.XtraBars.BarButtonItem();
            this.btnDong = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnChonFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoFile = new DevExpress.XtraEditors.SimpleButton();
            this.txtGhiChu = new System.Windows.Forms.RichTextBox();
            this.txtThongTinTaiSan = new System.Windows.Forms.RichTextBox();
            this.gcDuongSu = new DevExpress.XtraGrid.GridControl();
            this.gvDuongSu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colThuocBen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvLookupThuocBen = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colHoten = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvSearchDuongSu = new DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit();
            this.repositoryItemSearchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDuongSuHoTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuongSuSoGiayTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuongSuNamSinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuongSuDiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiayToTuyThan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoGiayTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNamSinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPopupGalleryEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupGalleryEdit();
            this.txtNoiDungCongChung = new System.Windows.Forms.RichTextBox();
            this.txtFileDinhKem = new DevExpress.XtraEditors.TextEdit();
            this.calPhiCongChung = new DevExpress.XtraEditors.CalcEdit();
            this.dtNgay = new DevExpress.XtraEditors.DateEdit();
            this.slookupLoaiHoSo = new DevExpress.XtraEditors.LookUpEdit();
            this.txtTenHoSo = new DevExpress.XtraEditors.TextEdit();
            this.txtSoHopDong = new DevExpress.XtraEditors.TextEdit();
            this.sLookupCongChungVien = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDuongSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDuongSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLookupThuocBen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSearchDuongSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupGalleryEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileDinhKem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calPhiCongChung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slookupLoaiHoSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHoSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sLookupCongChungVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnLuuVaTiepTuc,
            this.btnLuuVaDong,
            this.btnDong,
            this.btnThemDuongSuCaNhan,
            this.btnThemDuongSuToChuc});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 5;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLuuVaTiepTuc, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLuuVaDong, "", false, true, false, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThemDuongSuCaNhan, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnThemDuongSuToChuc, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDong, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnLuuVaTiepTuc
            // 
            this.btnLuuVaTiepTuc.Caption = "Lưu hồ sơ (Ctl + S)";
            this.btnLuuVaTiepTuc.Glyph = global::QuanLyPhongCongChung.Properties.Resources.save;
            this.btnLuuVaTiepTuc.Id = 0;
            this.btnLuuVaTiepTuc.Name = "btnLuuVaTiepTuc";
            this.btnLuuVaTiepTuc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLuuVaTiepTuc_ItemClick);
            // 
            // btnLuuVaDong
            // 
            this.btnLuuVaDong.Caption = "Lưu và đóng (Ctl + Shift + S)";
            this.btnLuuVaDong.Glyph = global::QuanLyPhongCongChung.Properties.Resources.refresh;
            this.btnLuuVaDong.Id = 1;
            this.btnLuuVaDong.Name = "btnLuuVaDong";
            this.btnLuuVaDong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLuuVaDong_ItemClick);
            // 
            // btnThemDuongSuCaNhan
            // 
            this.btnThemDuongSuCaNhan.Caption = "Thêm đương sự cá nhân (F4)";
            this.btnThemDuongSuCaNhan.Glyph = global::QuanLyPhongCongChung.Properties.Resources._1493302194_Plus;
            this.btnThemDuongSuCaNhan.Id = 3;
            this.btnThemDuongSuCaNhan.Name = "btnThemDuongSuCaNhan";
            this.btnThemDuongSuCaNhan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThemDuongSuCaNhan_ItemClick);
            // 
            // btnThemDuongSuToChuc
            // 
            this.btnThemDuongSuToChuc.Caption = "Thêm đương sự tổ chức (F6)";
            this.btnThemDuongSuToChuc.Glyph = global::QuanLyPhongCongChung.Properties.Resources._1493302194_Plus;
            this.btnThemDuongSuToChuc.Id = 4;
            this.btnThemDuongSuToChuc.Name = "btnThemDuongSuToChuc";
            this.btnThemDuongSuToChuc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThemDuongSuToChuc_ItemClick);
            // 
            // btnDong
            // 
            this.btnDong.Caption = "Đóng (Esc)";
            this.btnDong.Glyph = global::QuanLyPhongCongChung.Properties.Resources._1491322890_f_cross_256;
            this.btnDong.Id = 2;
            this.btnDong.Name = "btnDong";
            this.btnDong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDong_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(956, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 518);
            this.barDockControlBottom.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 486);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(956, 32);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 486);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnChonFile);
            this.layoutControl1.Controls.Add(this.btnMoFile);
            this.layoutControl1.Controls.Add(this.txtGhiChu);
            this.layoutControl1.Controls.Add(this.txtThongTinTaiSan);
            this.layoutControl1.Controls.Add(this.gcDuongSu);
            this.layoutControl1.Controls.Add(this.txtNoiDungCongChung);
            this.layoutControl1.Controls.Add(this.txtFileDinhKem);
            this.layoutControl1.Controls.Add(this.calPhiCongChung);
            this.layoutControl1.Controls.Add(this.dtNgay);
            this.layoutControl1.Controls.Add(this.slookupLoaiHoSo);
            this.layoutControl1.Controls.Add(this.txtTenHoSo);
            this.layoutControl1.Controls.Add(this.txtSoHopDong);
            this.layoutControl1.Controls.Add(this.sLookupCongChungVien);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(956, 486);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnChonFile
            // 
            this.btnChonFile.Location = new System.Drawing.Point(809, 452);
            this.btnChonFile.Name = "btnChonFile";
            this.btnChonFile.Size = new System.Drawing.Size(53, 22);
            this.btnChonFile.StyleController = this.layoutControl1;
            this.btnChonFile.TabIndex = 18;
            this.btnChonFile.Text = "Chọn file";
            this.btnChonFile.Click += new System.EventHandler(this.btnChonFile_Click);
            // 
            // btnMoFile
            // 
            this.btnMoFile.Location = new System.Drawing.Point(866, 452);
            this.btnMoFile.Name = "btnMoFile";
            this.btnMoFile.Size = new System.Drawing.Size(78, 22);
            this.btnMoFile.StyleController = this.layoutControl1;
            this.btnMoFile.TabIndex = 17;
            this.btnMoFile.Text = "Mở File";
            this.btnMoFile.Click += new System.EventHandler(this.btnMoFile_Click);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(12, 387);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(932, 61);
            this.txtGhiChu.TabIndex = 16;
            this.txtGhiChu.Text = "";
            // 
            // txtThongTinTaiSan
            // 
            this.txtThongTinTaiSan.Location = new System.Drawing.Point(12, 278);
            this.txtThongTinTaiSan.Name = "txtThongTinTaiSan";
            this.txtThongTinTaiSan.Size = new System.Drawing.Size(470, 89);
            this.txtThongTinTaiSan.TabIndex = 15;
            this.txtThongTinTaiSan.Text = "";
            // 
            // gcDuongSu
            // 
            this.gcDuongSu.Location = new System.Drawing.Point(12, 100);
            this.gcDuongSu.MainView = this.gvDuongSu;
            this.gcDuongSu.MenuManager = this.barManager1;
            this.gcDuongSu.Name = "gcDuongSu";
            this.gcDuongSu.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.gvLookupThuocBen,
            this.repositoryItemPopupGalleryEdit1,
            this.gvSearchDuongSu});
            this.gcDuongSu.Size = new System.Drawing.Size(932, 158);
            this.gcDuongSu.TabIndex = 14;
            this.gcDuongSu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDuongSu});
            this.gcDuongSu.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gcDuongSu_ProcessGridKey);
            // 
            // gvDuongSu
            // 
            this.gvDuongSu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colThuocBen,
            this.colHoten,
            this.colGiayToTuyThan,
            this.colSoGiayTo,
            this.colNamSinh,
            this.colDiaChi});
            this.gvDuongSu.GridControl = this.gcDuongSu;
            this.gvDuongSu.Name = "gvDuongSu";
            this.gvDuongSu.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gvDuongSu.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvDuongSu_CellValueChanged);
            // 
            // colThuocBen
            // 
            this.colThuocBen.Caption = "Thuộc Bên";
            this.colThuocBen.ColumnEdit = this.gvLookupThuocBen;
            this.colThuocBen.FieldName = "BenCongChung";
            this.colThuocBen.Name = "colThuocBen";
            this.colThuocBen.Visible = true;
            this.colThuocBen.VisibleIndex = 0;
            // 
            // gvLookupThuocBen
            // 
            this.gvLookupThuocBen.AutoHeight = false;
            this.gvLookupThuocBen.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gvLookupThuocBen.Name = "gvLookupThuocBen";
            this.gvLookupThuocBen.NullText = "Chọn bên...";
            // 
            // colHoten
            // 
            this.colHoten.Caption = "Họ tên";
            this.colHoten.ColumnEdit = this.gvSearchDuongSu;
            this.colHoten.FieldName = "DuongSuId";
            this.colHoten.Name = "colHoten";
            this.colHoten.Visible = true;
            this.colHoten.VisibleIndex = 1;
            // 
            // gvSearchDuongSu
            // 
            this.gvSearchDuongSu.AutoHeight = false;
            this.gvSearchDuongSu.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gvSearchDuongSu.Name = "gvSearchDuongSu";
            this.gvSearchDuongSu.NullText = "Chọn đương sự...";
            this.gvSearchDuongSu.View = this.repositoryItemSearchLookUpEdit1View;
            // 
            // repositoryItemSearchLookUpEdit1View
            // 
            this.repositoryItemSearchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDuongSuHoTen,
            this.colDuongSuSoGiayTo,
            this.colDuongSuNamSinh,
            this.colDuongSuDiaChi});
            this.repositoryItemSearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemSearchLookUpEdit1View.Name = "repositoryItemSearchLookUpEdit1View";
            this.repositoryItemSearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemSearchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colDuongSuHoTen
            // 
            this.colDuongSuHoTen.Caption = "Họ tên";
            this.colDuongSuHoTen.FieldName = "HoTen";
            this.colDuongSuHoTen.Name = "colDuongSuHoTen";
            this.colDuongSuHoTen.Visible = true;
            this.colDuongSuHoTen.VisibleIndex = 0;
            // 
            // colDuongSuSoGiayTo
            // 
            this.colDuongSuSoGiayTo.Caption = "Số giấy tờ";
            this.colDuongSuSoGiayTo.FieldName = "SoGiayToTuyThan";
            this.colDuongSuSoGiayTo.Name = "colDuongSuSoGiayTo";
            this.colDuongSuSoGiayTo.Visible = true;
            this.colDuongSuSoGiayTo.VisibleIndex = 1;
            // 
            // colDuongSuNamSinh
            // 
            this.colDuongSuNamSinh.Caption = "Năm sinh";
            this.colDuongSuNamSinh.FieldName = "NgaySinh";
            this.colDuongSuNamSinh.Name = "colDuongSuNamSinh";
            this.colDuongSuNamSinh.Visible = true;
            this.colDuongSuNamSinh.VisibleIndex = 2;
            // 
            // colDuongSuDiaChi
            // 
            this.colDuongSuDiaChi.Caption = "DiaChi";
            this.colDuongSuDiaChi.FieldName = "Địa chỉ";
            this.colDuongSuDiaChi.Name = "colDuongSuDiaChi";
            this.colDuongSuDiaChi.Visible = true;
            this.colDuongSuDiaChi.VisibleIndex = 3;
            // 
            // colGiayToTuyThan
            // 
            this.colGiayToTuyThan.Caption = "Loại giấy tờ";
            this.colGiayToTuyThan.FieldName = "LoaiGiayToTuyThan";
            this.colGiayToTuyThan.Name = "colGiayToTuyThan";
            this.colGiayToTuyThan.OptionsColumn.AllowEdit = false;
            this.colGiayToTuyThan.Visible = true;
            this.colGiayToTuyThan.VisibleIndex = 2;
            // 
            // colSoGiayTo
            // 
            this.colSoGiayTo.Caption = "Số giấy tờ";
            this.colSoGiayTo.FieldName = "SoGiayToTuyThan";
            this.colSoGiayTo.Name = "colSoGiayTo";
            this.colSoGiayTo.OptionsColumn.AllowEdit = false;
            this.colSoGiayTo.Visible = true;
            this.colSoGiayTo.VisibleIndex = 3;
            // 
            // colNamSinh
            // 
            this.colNamSinh.Caption = "Năm sinh";
            this.colNamSinh.FieldName = "NgaySinh";
            this.colNamSinh.Name = "colNamSinh";
            this.colNamSinh.OptionsColumn.AllowEdit = false;
            this.colNamSinh.Visible = true;
            this.colNamSinh.VisibleIndex = 4;
            // 
            // colDiaChi
            // 
            this.colDiaChi.Caption = "Địa chỉ";
            this.colDiaChi.FieldName = "DiaChi";
            this.colDiaChi.Name = "colDiaChi";
            this.colDiaChi.OptionsColumn.AllowEdit = false;
            this.colDiaChi.Visible = true;
            this.colDiaChi.VisibleIndex = 5;
            // 
            // repositoryItemPopupGalleryEdit1
            // 
            this.repositoryItemPopupGalleryEdit1.AutoHeight = false;
            this.repositoryItemPopupGalleryEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupGalleryEdit1.Name = "repositoryItemPopupGalleryEdit1";
            // 
            // txtNoiDungCongChung
            // 
            this.txtNoiDungCongChung.Location = new System.Drawing.Point(486, 278);
            this.txtNoiDungCongChung.Name = "txtNoiDungCongChung";
            this.txtNoiDungCongChung.Size = new System.Drawing.Size(458, 89);
            this.txtNoiDungCongChung.TabIndex = 13;
            this.txtNoiDungCongChung.Text = "";
            // 
            // txtFileDinhKem
            // 
            this.txtFileDinhKem.Enabled = false;
            this.txtFileDinhKem.Location = new System.Drawing.Point(116, 452);
            this.txtFileDinhKem.MenuManager = this.barManager1;
            this.txtFileDinhKem.Name = "txtFileDinhKem";
            this.txtFileDinhKem.Size = new System.Drawing.Size(689, 20);
            this.txtFileDinhKem.StyleController = this.layoutControl1;
            this.txtFileDinhKem.TabIndex = 12;
            // 
            // calPhiCongChung
            // 
            this.calPhiCongChung.Location = new System.Drawing.Point(535, 60);
            this.calPhiCongChung.MenuManager = this.barManager1;
            this.calPhiCongChung.Name = "calPhiCongChung";
            this.calPhiCongChung.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calPhiCongChung.Size = new System.Drawing.Size(409, 20);
            this.calPhiCongChung.StyleController = this.layoutControl1;
            this.calPhiCongChung.TabIndex = 9;
            // 
            // dtNgay
            // 
            this.dtNgay.EditValue = null;
            this.dtNgay.Location = new System.Drawing.Point(535, 36);
            this.dtNgay.MenuManager = this.barManager1;
            this.dtNgay.Name = "dtNgay";
            this.dtNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtNgay.Size = new System.Drawing.Size(409, 20);
            this.dtNgay.StyleController = this.layoutControl1;
            this.dtNgay.TabIndex = 7;
            // 
            // slookupLoaiHoSo
            // 
            this.slookupLoaiHoSo.Location = new System.Drawing.Point(116, 36);
            this.slookupLoaiHoSo.MenuManager = this.barManager1;
            this.slookupLoaiHoSo.Name = "slookupLoaiHoSo";
            this.slookupLoaiHoSo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.slookupLoaiHoSo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Ten", "Tên việc công chứng")});
            this.slookupLoaiHoSo.Properties.NullText = "Chọn việc công chứng....";
            this.slookupLoaiHoSo.Size = new System.Drawing.Size(311, 20);
            this.slookupLoaiHoSo.StyleController = this.layoutControl1;
            this.slookupLoaiHoSo.TabIndex = 6;
            // 
            // txtTenHoSo
            // 
            this.txtTenHoSo.Location = new System.Drawing.Point(535, 12);
            this.txtTenHoSo.MenuManager = this.barManager1;
            this.txtTenHoSo.Name = "txtTenHoSo";
            this.txtTenHoSo.Size = new System.Drawing.Size(409, 20);
            this.txtTenHoSo.StyleController = this.layoutControl1;
            this.txtTenHoSo.TabIndex = 5;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(116, 12);
            this.txtSoHopDong.MenuManager = this.barManager1;
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(311, 20);
            this.txtSoHopDong.StyleController = this.layoutControl1;
            this.txtSoHopDong.TabIndex = 4;
            // 
            // sLookupCongChungVien
            // 
            this.sLookupCongChungVien.Location = new System.Drawing.Point(116, 60);
            this.sLookupCongChungVien.MenuManager = this.barManager1;
            this.sLookupCongChungVien.Name = "sLookupCongChungVien";
            this.sLookupCongChungVien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sLookupCongChungVien.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HoTen", "Tên công chứng viên")});
            this.sLookupCongChungVien.Properties.NullText = "Chọn công chứng viên...";
            this.sLookupCongChungVien.Size = new System.Drawing.Size(311, 20);
            this.sLookupCongChungVien.StyleController = this.layoutControl1;
            this.sLookupCongChungVien.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(956, 486);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtSoHopDong;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(419, 24);
            this.layoutControlItem1.Text = "Số hợp đồng *";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTenHoSo;
            this.layoutControlItem2.Location = new System.Drawing.Point(419, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(517, 24);
            this.layoutControlItem2.Text = "Tên hồ sơ *";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.slookupLoaiHoSo;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(419, 24);
            this.layoutControlItem3.Text = "Việc công chứng *";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dtNgay;
            this.layoutControlItem4.Location = new System.Drawing.Point(419, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(517, 24);
            this.layoutControlItem4.Text = "Ngày công chứng";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.sLookupCongChungVien;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(419, 24);
            this.layoutControlItem5.Text = "Công chứng viên";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.calPhiCongChung;
            this.layoutControlItem6.Location = new System.Drawing.Point(419, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(517, 24);
            this.layoutControlItem6.Text = "Phí công chứng";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtNoiDungCongChung;
            this.layoutControlItem7.Location = new System.Drawing.Point(474, 250);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(462, 109);
            this.layoutControlItem7.Text = "Nội dung công chứng";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtFileDinhKem;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 440);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(797, 26);
            this.layoutControlItem9.Text = "File hợp đồng";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gcDuongSu;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(936, 178);
            this.layoutControlItem8.Text = "Đương sự liên quan";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtThongTinTaiSan;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 250);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(474, 109);
            this.layoutControlItem10.Text = "Thông tin tài sản";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtGhiChu;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 359);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(936, 81);
            this.layoutControlItem11.Text = "Ghi chú";
            this.layoutControlItem11.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(101, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnMoFile;
            this.layoutControlItem12.Location = new System.Drawing.Point(854, 440);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnChonFile;
            this.layoutControlItem13.Location = new System.Drawing.Point(797, 440);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(57, 26);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmCapNhatHoSo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 518);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmCapNhatHoSo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CẬP NHẬT HỒ SƠ CÔNG CHỨNG";
            this.Load += new System.EventHandler(this.frmCapNhatHoSo_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCapNhatHoSo_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDuongSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDuongSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLookupThuocBen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSearchDuongSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupGalleryEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileDinhKem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calPhiCongChung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slookupLoaiHoSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHoSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sLookupCongChungVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtTenHoSo;
        private DevExpress.XtraEditors.TextEdit txtSoHopDong;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DateEdit dtNgay;
        private DevExpress.XtraEditors.LookUpEdit slookupLoaiHoSo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.CalcEdit calPhiCongChung;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtFileDinhKem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.RichTextBox txtNoiDungCongChung;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraBars.BarButtonItem btnLuuVaTiepTuc;
        private DevExpress.XtraBars.BarButtonItem btnLuuVaDong;
        private DevExpress.XtraBars.BarButtonItem btnDong;
        private DevExpress.XtraGrid.GridControl gcDuongSu;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDuongSu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn colThuocBen;
        private DevExpress.XtraGrid.Columns.GridColumn colHoten;
        private DevExpress.XtraGrid.Columns.GridColumn colGiayToTuyThan;
        private DevExpress.XtraGrid.Columns.GridColumn colSoGiayTo;
        private DevExpress.XtraGrid.Columns.GridColumn colNamSinh;
        private DevExpress.XtraGrid.Columns.GridColumn colDiaChi;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit gvLookupThuocBen;
        private DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit gvSearchDuongSu;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemSearchLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupGalleryEdit repositoryItemPopupGalleryEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDuongSuHoTen;
        private DevExpress.XtraGrid.Columns.GridColumn colDuongSuSoGiayTo;
        private DevExpress.XtraGrid.Columns.GridColumn colDuongSuNamSinh;
        private DevExpress.XtraGrid.Columns.GridColumn colDuongSuDiaChi;
        private System.Windows.Forms.RichTextBox txtThongTinTaiSan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private System.Windows.Forms.RichTextBox txtGhiChu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.LookUpEdit sLookupCongChungVien;
        private DevExpress.XtraEditors.SimpleButton btnChonFile;
        private DevExpress.XtraEditors.SimpleButton btnMoFile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraBars.BarButtonItem btnThemDuongSuCaNhan;
        private DevExpress.XtraBars.BarButtonItem btnThemDuongSuToChuc;
    }
}