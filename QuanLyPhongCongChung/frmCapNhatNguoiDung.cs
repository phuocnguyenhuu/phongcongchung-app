﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyPhongCongChung
{
    public partial class frmCapNhatNguoiDung : DevExpress.XtraEditors.XtraForm
    {
        public frmCapNhatNguoiDung()
        {
            InitializeComponent();
        }

        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        public bool is_insert;
        public Guid Id_NguoiDung;
        public bool doimatkhau;
        private void frmCapNhatNguoiDung_Load(object sender, EventArgs e)
        {
            
            if (!is_insert)
            {
                fillDataUpdate();
                this.lgDoiMatKhau.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.layoutMatKhau.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            else
            {
                this.ckDoiMatKhau.Checked = true;
                this.lgDoiMatKhau.Expanded = false;
                this.lgDoiMatKhau.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            
        }
        
        private void fillDataUpdate()
        {
            if (!is_insert)
            {
                using (var db = new DataPhongCongChungDataContext())
                {
                    var _NguoiDung = db.NguoiDungs.Where(a => a.Id == Id_NguoiDung).SingleOrDefault();
                    if (_NguoiDung != null)
                    {
                        txtHoten.EditValue = _NguoiDung.HoTen;
                        txtTaikhoan.EditValue = _NguoiDung.UserName;
                        ckCongChungVien.EditValue = _NguoiDung.IsCongChungVien;
                        ckQuanTriVien.EditValue = _NguoiDung.IsAdmin;
                    }
                }
            }
        }
        private void btnLuu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            save();
          
        }
        private bool save()
        {
            string _HoTen = "";
            string _TaiKhoan = "";
            string _MatKhau = "";
            int count = 0;
            if (txtHoten.EditValue != null)
            {
                _HoTen = txtHoten.EditValue.ToString();
            }
            else
            {
                Utilities.ShowMessageError("Chưa  nhập tên người dùng");
                txtHoten.Focus();
                return false;
            }
            if (txtTaikhoan.EditValue != null)
            {
                _TaiKhoan = txtTaikhoan.EditValue.ToString();
            }
            else
            {
                Utilities.ShowMessageError("Chưa  nhập tài khoản");
                txtTaikhoan.Focus();
                return false;

            }

            if (doimatkhau)
            {
                if (!String.IsNullOrEmpty(txtMatkhau.EditValue.ToString()))
                {
                    _MatKhau = txtMatkhau.EditValue.ToString();
                }
                else
                {
                    Utilities.ShowMessageError("Chưa nhập mật khẩu");
                    txtMatkhau.Focus();
                    return false;
                }
            }
                
            try
            {
                using (var db = new DataPhongCongChungDataContext())
                {
                    count = db.NguoiDungs.Where(a => a.UserName == txtTaikhoan.EditValue.ToString() && a.Id != Id_NguoiDung).Count();
                    if (count <= 0)
                    {
                        NguoiDung _NguoiDung = null;
                        if (is_insert)
                        {
                            _NguoiDung = new NguoiDung();
                            _NguoiDung.Id = Guid.NewGuid();
                        }
                        else
                        {
                            _NguoiDung = db.NguoiDungs.Where(a=>a.Id == Id_NguoiDung).SingleOrDefault();
                        }
                        _NguoiDung.HoTen = _HoTen;
                        _NguoiDung.UserName = _TaiKhoan;
                        _NguoiDung.IsCongChungVien = ckCongChungVien.Checked;
                        _NguoiDung.IsAdmin = ckQuanTriVien.Checked;
                        _NguoiDung.IsDelete = false;
                        if (ckDoiMatKhau.Checked)
                        {
                            _NguoiDung.Password = EncryptionUtility.EncryptString(_MatKhau, Constants.keyMaHoa);
                        }
                        
                       
                        if (is_insert)
                        {
                            db.NguoiDungs.InsertOnSubmit(_NguoiDung);
                        }
                        db.SubmitChanges();
                        Utilities.ShowMessageSuccess("Lưu thành công");
                        return true;
                    }
                    else
                    {
                        Utilities.ShowMessageError(" Tài khoản người dùng đã tồn tại");
                        return false;
                    }
 
                }            
            }
            catch (Exception)
            {
                return false;

            }

        }

        private void btnThoat_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void frmCapNhatNguoiDung_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e, this);
            if (e.KeyData == (Keys.Control|Keys.S))
            {
                btnLuu_ItemClick(null, null);
            }
        }

        private void ckDoiMatKhau_CheckedChanged(object sender, EventArgs e)
        {
            if (ckDoiMatKhau.Checked)
            {
                this.layoutMatKhau.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                doimatkhau = true;
            }
            else
            {
                this.layoutMatKhau.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                doimatkhau = false;
            }
        }
    }
}
