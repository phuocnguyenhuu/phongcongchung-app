﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyPhongCongChung
{
    public partial class frmNguoiDung : DevExpress.XtraEditors.XtraForm
    {
        public frmNguoiDung()
        {
            InitializeComponent();
        }

        private void frmNguoiDung_Load(object sender, EventArgs e)
        {
            
            loadData();
            

        }

        private void loadData()
        {
            DataPhongCongChungDataContext db = new DataPhongCongChungDataContext();
            
                var dsNguoiDung = db.NguoiDungs.Where(a=>a.IsDelete == false).ToList();
                gcData.DataSource = dsNguoiDung;
                colId.Visible = false; 
            
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Xoa();

        }

        private void Xoa()
        {
            if (Utilities.ShowConfirmMessage("Bạn chắc chắn muốn xóa?"))
            {
                int row_index = gvData.FocusedRowHandle;
                string field_name = "Id";
                object value = gvData.GetRowCellValue(row_index, field_name);

                if (value != null)
                {
                    using (var db = new DataPhongCongChungDataContext())
                    {
                        Guid id = new Guid(value.ToString());
                        var _NguoiDung = db.NguoiDungs.Where(a => a.Id == id).SingleOrDefault();
                        if (_NguoiDung != null)
                        {
                            _NguoiDung.IsDelete = true;
                            db.SubmitChanges();
                            Utilities.ShowMessageSuccess("Đã xóa");
                            loadData();
                        }
                    }
                }
                else
                {
                    Utilities.ShowMessageError("Bạn chưa chọn dòng dữ liệu để xóa");

                }
            }
        }
      

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCapNhatNguoiDung _CapNhatNguoiDung = new frmCapNhatNguoiDung();
            _CapNhatNguoiDung.is_insert = true;
            _CapNhatNguoiDung.doimatkhau = true;
            _CapNhatNguoiDung.ShowDialog();
            loadData();
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCapNhatNguoiDung _CapNhatNguoiDung = new frmCapNhatNguoiDung();
            _CapNhatNguoiDung.is_insert = false;
            int row_index = gvData.FocusedRowHandle;
            string field_name = "Id";
            object value = gvData.GetRowCellValue(row_index, field_name);
            if (value != null)
            {
                _CapNhatNguoiDung.Id_NguoiDung = new Guid(value.ToString());
                _CapNhatNguoiDung.ShowDialog();
                loadData();
            }
            else
            {
                Utilities.ShowMessageError("Bạn chưa chọn dòng dữ liệu để sửa");
            }
        }

        private void btnCapNhat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadData();
        }

        private void gcData_Click(object sender, EventArgs e)
        {

        }

        private void frmNguoiDung_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e, this);
            if (e.KeyCode == Keys.F4)
            {
                btnThem_ItemClick(null, null);
            }
            if (e.KeyCode == Keys.F3)
            {
                btnSua_ItemClick(null, null);
            }
            if (e.KeyCode == Keys.F5)
            {
                btnCapNhat_ItemClick(null, null);
            }
            if (e.KeyCode == Keys.F8)
            {
                btnXoa_ItemClick(null, null);
            }
        }

        private void frmNguoiDung_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }
    }
}