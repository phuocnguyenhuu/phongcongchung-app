﻿namespace QuanLyPhongCongChung
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnCongChung = new DevExpress.XtraBars.BarButtonItem();
            this.btDuongSu = new DevExpress.XtraBars.BarButtonItem();
            this.btnHoSo = new DevExpress.XtraBars.BarButtonItem();
            this.btnDoiMatKhau = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuanLyNguoiDung = new DevExpress.XtraBars.BarButtonItem();
            this.btnTraCuuHoSo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnThoat = new DevExpress.XtraBars.BarButtonItem();
            this.btnTeamviwer = new DevExpress.XtraBars.BarButtonItem();
            this.btnUltraviewer = new DevExpress.XtraBars.BarButtonItem();
            this.btnAnyDesk = new DevExpress.XtraBars.BarButtonItem();
            this.btnTroGiupKyThuat = new DevExpress.XtraBars.BarButtonItem();
            this.btnDanhMucLoaiHoSo = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.pageGroupHoSo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageGroupDuongSu = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageGroupNguoiDung = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageGroupPhanMemDieuKhien = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageGroupTroGiup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.btnCongChung,
            this.btDuongSu,
            this.btnHoSo,
            this.btnDoiMatKhau,
            this.btnQuanLyNguoiDung,
            this.btnTraCuuHoSo,
            this.barButtonItem1,
            this.btnThoat,
            this.btnTeamviwer,
            this.btnUltraviewer,
            this.btnAnyDesk,
            this.btnTroGiupKyThuat,
            this.btnDanhMucLoaiHoSo});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 15;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(1133, 143);
            // 
            // btnCongChung
            // 
            this.btnCongChung.Caption = "Công chứng";
            this.btnCongChung.Id = 1;
            this.btnCongChung.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.file;
            this.btnCongChung.LargeWidth = 70;
            this.btnCongChung.Name = "btnCongChung";
            this.btnCongChung.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCongChung_ItemClick);
            // 
            // btDuongSu
            // 
            this.btDuongSu.Caption = "Đương sự tổ chức";
            this.btDuongSu.Id = 2;
            this.btDuongSu.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.network;
            this.btDuongSu.LargeWidth = 70;
            this.btDuongSu.Name = "btDuongSu";
            this.btDuongSu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btDuongSu_ItemClick);
            // 
            // btnHoSo
            // 
            this.btnHoSo.Caption = "Hố sơ công chứng";
            this.btnHoSo.Id = 3;
            this.btnHoSo.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.office_material;
            this.btnHoSo.LargeWidth = 70;
            this.btnHoSo.Name = "btnHoSo";
            this.btnHoSo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnHoSo_ItemClick);
            // 
            // btnDoiMatKhau
            // 
            this.btnDoiMatKhau.Caption = "Thay đổi mật khẩu";
            this.btnDoiMatKhau.Id = 4;
            this.btnDoiMatKhau.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.key;
            this.btnDoiMatKhau.LargeWidth = 70;
            this.btnDoiMatKhau.Name = "btnDoiMatKhau";
            this.btnDoiMatKhau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDoiMatKhau_ItemClick);
            // 
            // btnQuanLyNguoiDung
            // 
            this.btnQuanLyNguoiDung.Caption = "Người dùng";
            this.btnQuanLyNguoiDung.Id = 5;
            this.btnQuanLyNguoiDung.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.boy;
            this.btnQuanLyNguoiDung.LargeWidth = 70;
            this.btnQuanLyNguoiDung.Name = "btnQuanLyNguoiDung";
            this.btnQuanLyNguoiDung.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnQuanLyNguoiDung_ItemClick);
            // 
            // btnTraCuuHoSo
            // 
            this.btnTraCuuHoSo.Caption = "Tra cứu hồ sơ";
            this.btnTraCuuHoSo.Id = 6;
            this.btnTraCuuHoSo.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.search;
            this.btnTraCuuHoSo.LargeWidth = 70;
            this.btnTraCuuHoSo.Name = "btnTraCuuHoSo";
            this.btnTraCuuHoSo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTraCuuHoSo_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Đương sự cá nhân";
            this.barButtonItem1.Id = 7;
            this.barButtonItem1.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.man;
            this.barButtonItem1.LargeWidth = 70;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnThoat
            // 
            this.btnThoat.Caption = "Thoát";
            this.btnThoat.Id = 8;
            this.btnThoat.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.logout;
            this.btnThoat.LargeWidth = 70;
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThoat_ItemClick);
            // 
            // btnTeamviwer
            // 
            this.btnTeamviwer.Caption = "Teamviwer";
            this.btnTeamviwer.Id = 10;
            this.btnTeamviwer.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.if_teamviewer_100417;
            this.btnTeamviwer.LargeWidth = 70;
            this.btnTeamviwer.Name = "btnTeamviwer";
            this.btnTeamviwer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTeamviwer_ItemClick);
            // 
            // btnUltraviewer
            // 
            this.btnUltraviewer.Caption = "Ultraviewer";
            this.btnUltraviewer.Id = 11;
            this.btnUltraviewer.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.ultraviewer_87735;
            this.btnUltraviewer.LargeWidth = 70;
            this.btnUltraviewer.Name = "btnUltraviewer";
            this.btnUltraviewer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUltraviewer_ItemClick);
            // 
            // btnAnyDesk
            // 
            this.btnAnyDesk.Caption = "AnyDesk";
            this.btnAnyDesk.Id = 12;
            this.btnAnyDesk.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.dwh60RqR_400x400;
            this.btnAnyDesk.Name = "btnAnyDesk";
            this.btnAnyDesk.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAnyDesk_ItemClick);
            // 
            // btnTroGiupKyThuat
            // 
            this.btnTroGiupKyThuat.Caption = "Trợ giúp kỹ thuật";
            this.btnTroGiupKyThuat.Id = 13;
            this.btnTroGiupKyThuat.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.customer_service;
            this.btnTroGiupKyThuat.LargeWidth = 70;
            this.btnTroGiupKyThuat.Name = "btnTroGiupKyThuat";
            this.btnTroGiupKyThuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTroGiupKyThuat_ItemClick);
            // 
            // btnDanhMucLoaiHoSo
            // 
            this.btnDanhMucLoaiHoSo.Caption = "Loại hồ sơ";
            this.btnDanhMucLoaiHoSo.Id = 14;
            this.btnDanhMucLoaiHoSo.LargeGlyph = global::QuanLyPhongCongChung.Properties.Resources.diagram;
            this.btnDanhMucLoaiHoSo.LargeWidth = 70;
            this.btnDanhMucLoaiHoSo.Name = "btnDanhMucLoaiHoSo";
            this.btnDanhMucLoaiHoSo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDanhMucLoaiHoSo_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.pageGroupHoSo,
            this.pageGroupDuongSu,
            this.pageGroupNguoiDung,
            this.pageGroupPhanMemDieuKhien,
            this.pageGroupTroGiup});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Quản lý công chứng";
            // 
            // pageGroupHoSo
            // 
            this.pageGroupHoSo.ItemLinks.Add(this.btnCongChung);
            this.pageGroupHoSo.ItemLinks.Add(this.btnHoSo);
            this.pageGroupHoSo.ItemLinks.Add(this.btnTraCuuHoSo);
            this.pageGroupHoSo.ItemLinks.Add(this.btnDanhMucLoaiHoSo);
            this.pageGroupHoSo.Name = "pageGroupHoSo";
            this.pageGroupHoSo.Text = "Chức năng hệ thống";
            // 
            // pageGroupDuongSu
            // 
            this.pageGroupDuongSu.ItemLinks.Add(this.btDuongSu);
            this.pageGroupDuongSu.ItemLinks.Add(this.barButtonItem1);
            this.pageGroupDuongSu.Name = "pageGroupDuongSu";
            this.pageGroupDuongSu.Text = "Đương sự";
            // 
            // pageGroupNguoiDung
            // 
            this.pageGroupNguoiDung.ItemLinks.Add(this.btnQuanLyNguoiDung);
            this.pageGroupNguoiDung.ItemLinks.Add(this.btnDoiMatKhau);
            this.pageGroupNguoiDung.ItemLinks.Add(this.btnThoat);
            this.pageGroupNguoiDung.Name = "pageGroupNguoiDung";
            this.pageGroupNguoiDung.Text = "Người dùng";
            // 
            // pageGroupPhanMemDieuKhien
            // 
            this.pageGroupPhanMemDieuKhien.ItemLinks.Add(this.btnTeamviwer);
            this.pageGroupPhanMemDieuKhien.ItemLinks.Add(this.btnUltraviewer);
            this.pageGroupPhanMemDieuKhien.ItemLinks.Add(this.btnAnyDesk);
            this.pageGroupPhanMemDieuKhien.Name = "pageGroupPhanMemDieuKhien";
            this.pageGroupPhanMemDieuKhien.Text = "Phần mềm điều khiển từ xa";
            // 
            // pageGroupTroGiup
            // 
            this.pageGroupTroGiup.ItemLinks.Add(this.btnTroGiupKyThuat);
            this.pageGroupTroGiup.Name = "pageGroupTroGiup";
            this.pageGroupTroGiup.Text = "Trợ giúp kỹ thuật";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InTabControlHeader;
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // frmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1133, 577);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HỆ THỐNG QUẢN LÝ CÔNG CHỨNG";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup pageGroupHoSo;
        private DevExpress.XtraBars.BarButtonItem btnCongChung;
        private DevExpress.XtraBars.BarButtonItem btDuongSu;
        private DevExpress.XtraBars.BarButtonItem btnHoSo;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.BarButtonItem btnDoiMatKhau;
        private DevExpress.XtraBars.BarButtonItem btnQuanLyNguoiDung;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup pageGroupDuongSu;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup pageGroupNguoiDung;
        private DevExpress.XtraBars.BarButtonItem btnTraCuuHoSo;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnThoat;
        private DevExpress.XtraBars.BarButtonItem btnTeamviwer;
        private DevExpress.XtraBars.BarButtonItem btnUltraviewer;
        private DevExpress.XtraBars.BarButtonItem btnAnyDesk;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup pageGroupPhanMemDieuKhien;
        private DevExpress.XtraBars.BarButtonItem btnTroGiupKyThuat;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup pageGroupTroGiup;
        private DevExpress.XtraBars.BarButtonItem btnDanhMucLoaiHoSo;
    }
}

