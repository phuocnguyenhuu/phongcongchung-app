﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongCongChung
{
    public partial class frmCapNhatHoSo : DevExpress.XtraEditors.XtraForm
    {
        public bool isAdd = true;
        public Guid Id = Guid.Empty;
        public frmCapNhatHoSo()
        {
            InitializeComponent();            
        }
        private void frmCapNhatHoSo_Load(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            dtNgay.EditValue = now;
            LoadLoaiHoSo();            
            LoadThuocBen();
            LoadDuongSu();
            initGridDuongDuLienQuan();
            LoadCongChungVien();
            btnMoFile.Enabled = false;
            if (!isAdd)
            {
                FillData();
                if (!String.Empty.Equals(Utilities.GetValueFromObjectInput(txtFileDinhKem.EditValue)))
                {
                    btnMoFile.Enabled = true;
                }
            }
            else
            {
                if (LoginInfo.nguoiDungInfo != null)
                {
                    sLookupCongChungVien.EditValue = LoginInfo.nguoiDungInfo.HoTen;
                }
            }

        }
        private void gvDuongSu_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "DuongSuId" )
            {
                object duongSuId = gvDuongSu.GetRowCellValue(e.RowHandle, "DuongSuId");
                if (duongSuId != null)
                {
                    Guid guidDuongSuId = Utilities.GetGuidFromObjectInput(duongSuId);
                    using (var db = new DataPhongCongChungDataContext())
                    {
                        var duongSu = db.DuongSus.Where(a => a.Id == guidDuongSuId).SingleOrDefault();
                        if (duongSu != null)
                        {
                            gvDuongSu.SetRowCellValue(gvDuongSu.FocusedRowHandle, "LoaiGiayToTuyThan", duongSu.LoaiGiayToTuyThan);
                            gvDuongSu.SetRowCellValue(gvDuongSu.FocusedRowHandle, "SoGiayToTuyThan", duongSu.SoGiayToTuyThan);
                            gvDuongSu.SetRowCellValue(gvDuongSu.FocusedRowHandle, "NgaySinh", duongSu.NgaySinh);
                            gvDuongSu.SetRowCellValue(gvDuongSu.FocusedRowHandle, "DiaChi", duongSu.DiaChi);
                        }
                    }
                }
            }
        }
        private void initGridDuongDuLienQuan()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BenCongChung", typeof(string));
            dt.Columns.Add("DuongSuId", typeof(Guid));
            dt.Columns.Add("LoaiGiayToTuyThan", typeof(string));
            dt.Columns.Add("SoGiayToTuyThan", typeof(string));
            dt.Columns.Add("NgaySinh", typeof(string));
            dt.Columns.Add("DiaChi", typeof(string));
            using (var db = new DataPhongCongChungDataContext())
            {
                var ds = db.sp_getListDuongSuCongChungHoSo(Id).ToList();
                if (ds.Count > 0)
                {
                    foreach (var item in ds)
                    {
                        dt.Rows.Add(item.BenCongChung, item.DuongSuId, item.LoaiGiayToTuyThan, item.SoGiayToTuyThan, item.NgaySinh, item.DiaChi);
                    }
                }
            }
            gcDuongSu.DataSource = dt;
        }

        private void initEmptyGridDuongDuLienQuan()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BenCongChung", typeof(string));
            dt.Columns.Add("DuongSuId", typeof(Guid));
            dt.Columns.Add("LoaiGiayToTuyThan", typeof(string));
            dt.Columns.Add("SoGiayToTuyThan", typeof(string));
            dt.Columns.Add("NgaySinh", typeof(string));
            dt.Columns.Add("DiaChi", typeof(string));
            
            gcDuongSu.DataSource = dt;
        }

        private void LoadDuongSu()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                var ds = db.sp_getDanhSachDuongSu().ToList();
                gvSearchDuongSu.DataSource = ds;
                gvSearchDuongSu.ValueMember = "Id";
                gvSearchDuongSu.DisplayMember = "HoTen";
            }
        }

        private void LoadLoaiHoSo()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                var loaiHoSos = db.LoaiHoSos.Where(a => a.IsDeleled == false).OrderBy(a => a.Ten).ToList();
                slookupLoaiHoSo.Properties.DataSource = loaiHoSos;
                slookupLoaiHoSo.Properties.DisplayMember = "Ten";
                slookupLoaiHoSo.Properties.ValueMember = "Id";
            }
        }
        private void LoadCongChungVien()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                var congChungViens = db.NguoiDungs.Where(a => a.IsDelete == false && a.IsCongChungVien == true).OrderBy(a => a.HoTen).ToList();
                sLookupCongChungVien.Properties.DataSource = congChungViens;
                sLookupCongChungVien.Properties.DisplayMember = "HoTen";
                sLookupCongChungVien.Properties.ValueMember = "HoTen";
            }
        }
        private void LoadThuocBen()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Ben", typeof(string));
            dt.Rows.Add("A");
            dt.Rows.Add("B");
            dt.Rows.Add("C");
            gvLookupThuocBen.DataSource = dt;
            gvLookupThuocBen.DisplayMember = "Ben";
            gvLookupThuocBen.ValueMember = "Ben";
        }

        private void frmCapNhatHoSo_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e, this);
            if (e.Control && e.KeyCode == Keys.S)
            {
                btnLuuVaTiepTuc_ItemClick(null, null);
            }
            else if (e.Control && e.Shift && e.KeyCode == Keys.S)
            {
                btnLuuVaDong_ItemClick(null, null);
            }
            else if (e.KeyCode == Keys.F4)
            {
                frmUpdateDuongSu frm = new frmUpdateDuongSu();
                frm.is_insert = true;
                frm.loaiduongsu = "CANHAN";
                frm.ShowDialog();
                LoadDuongSu();
            }
            else if (e.KeyCode == Keys.F6)
            {
                frmUpdateDuongSu frm = new frmUpdateDuongSu();
                frm.is_insert = true;
                frm.loaiduongsu = "TOCHUC";
                frm.ShowDialog();
                LoadDuongSu();
            }
        }

        private void btnDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
        private bool validateInput()
        {
            if (String.Empty.Equals(Utilities.GetStringFromObjectInput(txtSoHopDong.EditValue)))
            {
                Utilities.ShowMessageError("Chưa nhập số hợp đồng");
                txtSoHopDong.Focus();
                return false;
            }
            if (String.Empty.Equals(Utilities.GetStringFromObjectInput(txtTenHoSo.EditValue)))
            {
                Utilities.ShowMessageError("Chưa nhập tên hồ sơ");
                txtTenHoSo.Focus();
                return false;
            }
            if (String.Empty.Equals(Utilities.GetStringFromObjectInput(slookupLoaiHoSo.EditValue)))
            {
                Utilities.ShowMessageError("Chưa chọn việc công chứng");
                slookupLoaiHoSo.Focus();
                return false;
            }
            if (gvDuongSu.RowCount-1 <= 0)
            {
                Utilities.ShowMessageError("Chưa chọn đương sự liên quan để công chứng");
                gvDuongSu.Focus();
                return false;
            }
            return true;
        }
        private bool SaveData()
        {
            try
            {
                using (var db = new DataPhongCongChungDataContext())
                {
                    HoSo hoSo = null;
                    DuongSuCongChungHoSo duongSuCongChungHoSo = null;
                    Guid id = Guid.NewGuid();
                    DateTime now = DateTime.Now;
                    if (isAdd)
                    {
                        hoSo = new HoSo();
                        hoSo.Id = id;
                        hoSo.CreatedBy = LoginInfo.nguoiDungInfo.UserName;
                        hoSo.CreatedDate = now;
                        hoSo.IsDeleted = false;
                    }
                    else
                    {
                        hoSo = db.HoSos.Where(a => a.Id == this.Id).SingleOrDefault();
                    }
                    hoSo.TenHopDong = Utilities.GetStringFromObjectInput(txtTenHoSo.EditValue);
                    hoSo.SoCongChung = Utilities.GetStringFromObjectInput(txtSoHopDong.EditValue);
                    hoSo.NgayLap = Utilities.GetDateTimeFromObjectInput(dtNgay.EditValue);
                    hoSo.HoTenCongChungVien = Utilities.GetStringFromObjectInput(sLookupCongChungVien.EditValue);
                    hoSo.NoiDungHoSo = txtNoiDungCongChung.Text;
                    hoSo.GhiChu = txtGhiChu.Text;
                    hoSo.ThongTinTaiSan = txtThongTinTaiSan.Text;
                    hoSo.LoaiHoSoId = Utilities.GetGuidFromObjectInput(slookupLoaiHoSo.EditValue);
                    hoSo.GiaCongChung = Convert.ToDecimal(calPhiCongChung.EditValue);
                    hoSo.FileDinhKem = Utilities.GetStringFromObjectInput(txtFileDinhKem.EditValue);
                    hoSo.UpdatedBy = LoginInfo.nguoiDungInfo.UserName;
                    hoSo.UpdatedDate = now;
                    int rowCount = gvDuongSu.RowCount - 1;
                    if (!isAdd)
                    {
                        var deleteDuongSuCongChungHoSos = db.DuongSuCongChungHoSos.Where(a => a.HoSoId == hoSo.Id).ToList();
                        db.DuongSuCongChungHoSos.DeleteAllOnSubmit(deleteDuongSuCongChungHoSos);
                    }
                    for (int i = 0; i < rowCount; i++)
                    {
                        duongSuCongChungHoSo = new DuongSuCongChungHoSo();
                        string benCongChung = Utilities.GetStringFromObjectInput(gvDuongSu.GetRowCellValue(i, "BenCongChung"));
                        Guid duongSuId = Utilities.GetGuidFromObjectInput(gvDuongSu.GetRowCellValue(i, "DuongSuId"));
                        if (!String.Empty.Equals(benCongChung) && Guid.Empty != duongSuId)
                        {
                            duongSuCongChungHoSo.Id = Guid.NewGuid();
                            duongSuCongChungHoSo.HoSoId = hoSo.Id;
                            duongSuCongChungHoSo.DuongSuId = duongSuId;
                            duongSuCongChungHoSo.BenCongChung = benCongChung;
                            duongSuCongChungHoSo.CreatedBy = LoginInfo.nguoiDungInfo.UserName;
                            duongSuCongChungHoSo.CreatedDate = now;
                            duongSuCongChungHoSo.UpdatedBy = LoginInfo.nguoiDungInfo.UserName;
                            duongSuCongChungHoSo.UpdatedDate = now;
                            duongSuCongChungHoSo.IsDelete = false;
                            db.DuongSuCongChungHoSos.InsertOnSubmit(duongSuCongChungHoSo);
                        }
                    }
                    if (isAdd)
                    {
                        db.HoSos.InsertOnSubmit(hoSo);
                    }
                    db.SubmitChanges();
                    return true;
                }
            }
            catch (Exception)
            {
            }  
            return false;
        }
        private void FillData()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                var hoSo = db.HoSos.Where(a => a.Id == this.Id).SingleOrDefault();
                if (hoSo != null)
                {
                    txtTenHoSo.EditValue = hoSo.TenHopDong;
                    txtSoHopDong.EditValue = hoSo.SoCongChung;
                    dtNgay.EditValue = hoSo.NgayLap;
                    sLookupCongChungVien.EditValue = hoSo.HoTenCongChungVien;
                    txtNoiDungCongChung.Text = hoSo.NoiDungHoSo;
                    txtGhiChu.Text = hoSo.GhiChu;
                    txtThongTinTaiSan.Text = hoSo.ThongTinTaiSan;
                    slookupLoaiHoSo.EditValue = hoSo.LoaiHoSoId;
                    calPhiCongChung.EditValue = hoSo.GiaCongChung;
                    txtFileDinhKem.EditValue = hoSo.FileDinhKem;
                }
            }
        }
        private void ClearData()
        {
            txtSoHopDong.EditValue = null;
            txtTenHoSo.EditValue = null;
            txtThongTinTaiSan.Text = "";
            txtNoiDungCongChung.Text = "";
            txtGhiChu.Text = "";
            txtFileDinhKem.EditValue = null;
            calPhiCongChung.EditValue = null;
            initEmptyGridDuongDuLienQuan();

        }
        private void btnLuuVaTiepTuc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (validateInput())
            {
                if (SaveData())
                {
                    Utilities.ShowMessageSuccess("Cập nhật hồ sơ thành công");
                    if (isAdd)
                    {
                        ClearData();
                        txtSoHopDong.Focus();
                    }
                }
                else
                {
                    Utilities.ShowMessageError("Không thể cập nhật hồ sơ");
                }
            }
        }
        private void gcDuongSu_ProcessGridKey(object sender, KeyEventArgs e)
        {
            var grid = sender as GridControl;
            var view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
            }
        }

        private void btnLuuVaDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (validateInput())
            {
                if (SaveData())
                {
                    this.Close();
                }
            }
        }

        private void btnChonFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Title = "Chọn file hợp đồng";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFileDinhKem.Text = openFileDialog1.FileName;
            }
        }

        private void btnMoFile_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(txtFileDinhKem.Text))
            {
                System.Diagnostics.Process.Start(txtFileDinhKem.Text);
            }
            else
            {
                Utilities.ShowMessageError("File hợp đồng không tồn tại");
            }
        }

        private void btnThemDuongSuCaNhan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmUpdateDuongSu frm = new frmUpdateDuongSu();
            frm.is_insert = true;
            frm.loaiduongsu = "CANHAN";
            frm.ShowDialog();
        }

        private void btnThemDuongSuToChuc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmUpdateDuongSu frm = new frmUpdateDuongSu();
            frm.is_insert = true;
            frm.loaiduongsu = "TOCHUC";
            frm.ShowDialog();
        }
    }
}
