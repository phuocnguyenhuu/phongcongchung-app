﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
namespace QuanLyPhongCongChung
{
    public partial class frmTraCuuHoSo : DevExpress.XtraEditors.XtraForm
    {
        public frmTraCuuHoSo()
        {
            InitializeComponent();
        }

        private void frmTraCuuHoSo_Load(object sender, EventArgs e)
        {
            LoadDuongSu();
        }

        private void LoadData()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                Guid duongSuId = Guid.Empty;
                if (slDuongSu.EditValue != null)
                {
                    if (!String.Empty.Equals(slDuongSu.EditValue.ToString()))
                    {
                        duongSuId = Utilities.GetGuidFromObjectInput(slDuongSu.EditValue);
                    }
                }
                var list = db.sp_getDanhSachHoSo(duongSuId).Where(a => true);

                string tenHopDong = Utilities.GetValueFromObjectInput(txtTenHoSo.EditValue);
                string soCongChung = Utilities.GetValueFromObjectInput(txtSoCongChung.EditValue);
                string congChungVien = Utilities.GetValueFromObjectInput(txtCongChungVien.EditValue);
                string noiDungHoSo = Utilities.GetValueFromObjectInput(txtNoiDungHopDong.EditValue);
                string ghiChu = Utilities.GetValueFromObjectInput(txtThongTinGhiChu.EditValue);
                string thongTinTaiSan = Utilities.GetValueFromObjectInput(txtThongTinTaiSan.EditValue);

                if (dtTuNgay.EditValue != null)
                {
                    list = list.Where(a => a.NgayLap >= Convert.ToDateTime(dtTuNgay.EditValue));
                }
                if (dtDenNgay.EditValue != null)
                {
                    list = list.Where(a => a.NgayLap <= Utilities.GetDenNgay(Convert.ToDateTime(dtDenNgay.EditValue)));
                }
                if (!String.Empty.Equals(tenHopDong))
                {
                    list = list.Where(a => a.TenHopDong != null && a.TenHopDong.ToLower().Contains(tenHopDong.ToLower()));
                }
                if (!String.Empty.Equals(soCongChung))
                {
                    list = list.Where(a => a.SoCongChung != null && a.SoCongChung.ToLower().Contains(soCongChung.ToLower()));
                }
                if (!String.Empty.Equals(congChungVien))
                {
                    list = list.Where(a => a.HoTenCongChungVien != null && a.HoTenCongChungVien.ToLower().Contains(congChungVien.ToLower()));
                }
                if (!String.Empty.Equals(noiDungHoSo))
                {
                    list = list.Where(a => a.NoiDungHoSo != null && a.NoiDungHoSo.ToLower().Contains(noiDungHoSo.ToLower()));
                }
                if (!String.Empty.Equals(ghiChu))
                {
                    list = list.Where(a => a.GhiChu != null && a.GhiChu.ToLower().Contains(ghiChu.ToLower()));
                }
                if (!String.Empty.Equals(thongTinTaiSan))
                {
                    list = list.Where(a => a.ThongTinTaiSan != null && a.ThongTinTaiSan.ToLower().Contains(thongTinTaiSan.ToLower()));
                }

                gcData.DataSource = list.ToList();
            }

        }
        private void LoadDuongSu()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                slDuongSu.Properties.DataSource = db.sp_getDanhSachDuongSu().ToList();
                slDuongSu.Properties.DisplayMember = "HoTen";
                slDuongSu.Properties.ValueMember = "Id";
            }

        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void frmTraCuuHoSo_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e,this);
            if (e.KeyCode == Keys.Enter)
            {
                LoadData();
            }
        }

        private void btnXuatExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //string folderName = "";
            //DialogResult result = folderBrowserDialog1.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    folderName = folderBrowserDialog1.SelectedPath;
            //    string FileName = folderName + "\\DanhSachHoSo.xls";
            //    gvData.ExportToXls(FileName, Utilities.printingOptionFormat());
            //}

            using (var db = new DataPhongCongChungDataContext())
            {
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                Guid duongSuId = Guid.Empty;
                if (slDuongSu.EditValue != null)
                {
                    if (!String.Empty.Equals(slDuongSu.EditValue.ToString()))
                    {
                        duongSuId = Utilities.GetGuidFromObjectInput(slDuongSu.EditValue);
                    }
                }
                var list = db.sp_getDanhSachHoSo(duongSuId).Where(a => true);

                string tenHopDong = Utilities.GetValueFromObjectInput(txtTenHoSo.EditValue);
                string soCongChung = Utilities.GetValueFromObjectInput(txtSoCongChung.EditValue);
                string congChungVien = Utilities.GetValueFromObjectInput(txtCongChungVien.EditValue);
                string noiDungHoSo = Utilities.GetValueFromObjectInput(txtNoiDungHopDong.EditValue);
                string ghiChu = Utilities.GetValueFromObjectInput(txtThongTinGhiChu.EditValue);
                string thongTinTaiSan = Utilities.GetValueFromObjectInput(txtThongTinTaiSan.EditValue);

                if (dtTuNgay.EditValue != null)
                {
                    list = list.Where(a => a.NgayLap >= Convert.ToDateTime(dtTuNgay.EditValue));
                }
                if (dtDenNgay.EditValue != null)
                {
                    list = list.Where(a => a.NgayLap <= Utilities.GetDenNgay(Convert.ToDateTime(dtDenNgay.EditValue)));
                }
                if (!String.Empty.Equals(tenHopDong))
                {
                    list = list.Where(a => a.TenHopDong != null && a.TenHopDong.ToLower().Contains(tenHopDong.ToLower()));
                }
                if (!String.Empty.Equals(soCongChung))
                {
                    list = list.Where(a => a.SoCongChung != null && a.SoCongChung.ToLower().Contains(soCongChung.ToLower()));
                }
                if (!String.Empty.Equals(congChungVien))
                {
                    list = list.Where(a => a.HoTenCongChungVien != null && a.HoTenCongChungVien.ToLower().Contains(congChungVien.ToLower()));
                }
                if (!String.Empty.Equals(noiDungHoSo))
                {
                    list = list.Where(a => a.NoiDungHoSo != null && a.NoiDungHoSo.ToLower().Contains(noiDungHoSo.ToLower()));
                }
                if (!String.Empty.Equals(ghiChu))
                {
                    list = list.Where(a => a.GhiChu != null && a.GhiChu.ToLower().Contains(ghiChu.ToLower()));
                }
                if (!String.Empty.Equals(thongTinTaiSan))
                {
                    list = list.Where(a => a.ThongTinTaiSan != null && a.ThongTinTaiSan.ToLower().Contains(thongTinTaiSan.ToLower()));
                }


                int colIndexStt = 1;
                int colIndexSoCongChung = 2;
                int colIndexNgay = 3;
                int colIndexTenHopDong = 4;
                int colIndexDuongSu = 5;
                int colIndexTaiSan = 6;
                int colIndexNoiDung = 7;
                int colIndexGhiChu = 8;
                int colIndexCongChungVien = 9;
                int colIndexPhongCongChung = 10;

                xlWorkSheet.Cells[1, colIndexStt] = "STT";
                xlWorkSheet.Cells[1, colIndexSoCongChung] = "Số công chứng";
                xlWorkSheet.Cells[1, colIndexNgay] = "Ngày";
                xlWorkSheet.Cells[1, colIndexTenHopDong] = "Tên hợp đồng";
                xlWorkSheet.Cells[1, colIndexDuongSu] = "Đương sự liên quan";
                xlWorkSheet.Cells[1, colIndexTaiSan] = "Tài sản liên quan";
                xlWorkSheet.Cells[1, colIndexNoiDung] = "Nội dung";
                xlWorkSheet.Cells[1, colIndexGhiChu] = "Ghi chú";
                xlWorkSheet.Cells[1, colIndexCongChungVien] = "Công chứng viên";
                xlWorkSheet.Cells[1, colIndexPhongCongChung] = "Phòng công chứng";

                xlWorkSheet.Columns[colIndexStt].ColumnWidth = 5;
                xlWorkSheet.Columns[colIndexSoCongChung].ColumnWidth = 13;
                xlWorkSheet.Columns[colIndexNgay].ColumnWidth = 12;
                xlWorkSheet.Columns[colIndexTenHopDong].ColumnWidth = 30;
                xlWorkSheet.Columns[colIndexDuongSu].ColumnWidth = 30;
                xlWorkSheet.Columns[colIndexTaiSan].ColumnWidth = 30;
                xlWorkSheet.Columns[colIndexNoiDung].ColumnWidth = 30;
                xlWorkSheet.Columns[colIndexGhiChu].ColumnWidth = 30;
                xlWorkSheet.Columns[colIndexCongChungVien].ColumnWidth = 18;
                xlWorkSheet.Columns[colIndexPhongCongChung].ColumnWidth = 34;

                int rowindex = 2;
                int stt = 1;
                foreach (var item in list)
                {
                    xlWorkSheet.Cells[rowindex, colIndexStt] = stt++;
                    xlWorkSheet.Cells[rowindex, colIndexSoCongChung] = item.SoCongChung.ToString();
                    xlWorkSheet.Cells[rowindex, colIndexNgay] = item.NgayLap.ToString("dd/MM/yyyy");
                    xlWorkSheet.Cells[rowindex, colIndexTenHopDong] = item.TenHopDong.ToString();
                    var dsDuongSuLienQuan = db.DuongSuCongChungHoSos.Where(a => a.HoSoId == item.Id).OrderBy(a => a.BenCongChung).ToList();
                    string duongSuBenA = "";
                    string duongSuBenB = "";
                    string duongSuBenC = "";
                    foreach (var itemDuongSuLienQuan in dsDuongSuLienQuan)
                    {
                        var objDuongSu = db.DuongSus.Where(a => a.Id == itemDuongSuLienQuan.DuongSuId).SingleOrDefault();
                        if (objDuongSu != null)
                        {
                            string loaiDoiTuong = (objDuongSu.LoaiDoiTuong == "CANHAN" ? "Họ tên: " : "Đơn vị: ");
                            string soGiayToTuyThan = (objDuongSu.LoaiDoiTuong == "CANHAN" ? (objDuongSu.LoaiGiayToTuyThan + ": " + objDuongSu.SoGiayToTuyThan) : "MST: " + objDuongSu.MST);
                            if (itemDuongSuLienQuan.BenCongChung == "A")
                            {
                                duongSuBenA += loaiDoiTuong + objDuongSu.HoTen + "; " + soGiayToTuyThan + ": " + objDuongSu.SoGiayToTuyThan + (char)10 + "và ";
                            }
                            if (itemDuongSuLienQuan.BenCongChung == "B")
                            {
                                duongSuBenB += loaiDoiTuong + objDuongSu.HoTen + "; " + soGiayToTuyThan + ": " + objDuongSu.SoGiayToTuyThan + (char)10 + "và ";
                            }
                            if (itemDuongSuLienQuan.BenCongChung == "C")
                            {
                                duongSuBenC += loaiDoiTuong + objDuongSu.HoTen + "; " + soGiayToTuyThan + ": " + objDuongSu.SoGiayToTuyThan + (char)10 + "và ";
                            }
                        }
                    }
                    string duongSuLienQuan = "";
                    if (duongSuBenA != "")
                    {
                        duongSuLienQuan += "Bên A: " + (char)10 + ((duongSuBenA.Length >= 3) ? duongSuBenA.Remove(duongSuBenA.Length - 3) : duongSuBenA);
                    }
                    if (duongSuBenB != "")
                    {
                        duongSuLienQuan += "Bên B: " + (char)10 + ((duongSuBenB.Length >= 3) ? duongSuBenB.Remove(duongSuBenB.Length - 3) : duongSuBenB);
                    }
                    if (duongSuBenC != "")
                    {
                        duongSuLienQuan += "Bên C: " + (char)10 + ((duongSuBenC.Length >= 3) ? duongSuBenC.Remove(duongSuBenC.Length - 3) : duongSuBenC);
                    }
                    xlWorkSheet.Cells[rowindex, colIndexDuongSu] = duongSuLienQuan;
                    xlWorkSheet.Cells[rowindex, colIndexTaiSan] = item.ThongTinTaiSan;
                    xlWorkSheet.Cells[rowindex, colIndexNoiDung] = item.NoiDungHoSo;
                    xlWorkSheet.Cells[rowindex, colIndexGhiChu] = item.GhiChu;
                    xlWorkSheet.Cells[rowindex, colIndexCongChungVien] = item.HoTenCongChungVien;
                    //var donVi = db.ThongTinDonVis.Where(a => a.Id == new Guid("85D14D81-BCF4-4117-BB5C-CFA6E60745DA")).SingleOrDefault();
                    xlWorkSheet.Cells[rowindex, colIndexPhongCongChung] = "";
                    rowindex++;
                }
                Excel.Range chartRange;
                chartRange = xlWorkSheet.get_Range("A1:J" + (rowindex - 1));
                foreach (Excel.Range cell in chartRange.Cells)
                {
                    cell.BorderAround();
                    cell.WrapText = true;
                    cell.Font.Size = 11;
                    cell.Font.Name = "Times New Roman";
                }

                string folderName = "";
                DialogResult result = folderBrowserDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    folderName = folderBrowserDialog1.SelectedPath;

                }
                string FileName = folderName + "\\DanhSachHoSo.xls";
                xlWorkBook.SaveAs(FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                if (Utilities.ShowConfirmMessage("Bạn có muốn mở file excel vừa export"))
                {
                    FileInfo fi = new FileInfo(FileName);
                    if (fi.Exists)
                    {
                        System.Diagnostics.Process.Start(FileName);
                    }
                }
            }
            
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
