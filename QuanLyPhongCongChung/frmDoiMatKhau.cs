﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongCongChung
{
    public partial class frmDoiMatKhau : DevExpress.XtraEditors.XtraForm
    {
        public frmDoiMatKhau()
        {
            InitializeComponent();
        }

        private void doiMatKhau()
        {
            if (String.Empty.Equals(txtMatKhauCu.Text))
            {
                Utilities.ShowMessageError("Chưa nhập mật khẩu cũ");
                return;
            }
            else if (String.Empty.Equals(txtMatKhauMoi.Text))
            {
                Utilities.ShowMessageError("Chưa nhập mật khẩu mới");
                return;
            }
            else if (String.Empty.Equals(txtNhapLaiMatKhauMoi.Text))
            {
                Utilities.ShowMessageError("Chưa nhập lại mật khẩu mới");
                return;
            }else if(txtMatKhauMoi.Text!=txtNhapLaiMatKhauMoi.Text){
                Utilities.ShowMessageError("Nhập lại mật khẩu không đúng");
                return;
            }
            else
            {
                if (CheckMatKhauCuFromDb(txtMatKhauCu.Text))
                {
                    if (UpdateMatKhauMoi(txtMatKhauMoi.Text))
                    {
                        Utilities.ShowMessageSuccess("Đã cập nhật mật khẩu thành công");
                        this.Close();
                    }
                    else
                    {
                        Utilities.ShowMessageError("Không thể đổi mật khẩu");
                    }
                }
                else
                {
                    Utilities.ShowMessageError("Mật khẩu cũ không đúng");
                    return;
                }
            }
        }
        private bool CheckMatKhauCuFromDb(string password)
        {
            string matKhauDaMaKhoa = maHoaMatKhau(password);
            if (matKhauDaMaKhoa.Equals(LoginInfo.nguoiDungInfo.Password))
            {
                return true;
            }
            return false;
        }
        private bool UpdateMatKhauMoi(string password)
        {
            try
            {
                using (var db = new DataPhongCongChungDataContext())
                {

                    var nguoiDung = db.NguoiDungs.Where(a => a.Id == LoginInfo.nguoiDungInfo.Id).SingleOrDefault();
                    if (nguoiDung != null)
                    {
                        nguoiDung.Password = maHoaMatKhau(password);
                        db.SubmitChanges();
                        return true;
                    }
                }
            }
            catch (Exception)
            {
            }
            return false;
            
        }
        private string maHoaMatKhau(string password)
        {
            return EncryptionUtility.EncryptString(password, Constants.keyMaHoa);
        }

        private void frmDoiMatKhau_KeyDown(object sender, KeyEventArgs e)
        {
            Utilities.HotKeyDongForm(e, this);
            if (e.KeyCode == Keys.Enter)
            {
                doiMatKhau();
            }
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            doiMatKhau();
        }
    }
}
