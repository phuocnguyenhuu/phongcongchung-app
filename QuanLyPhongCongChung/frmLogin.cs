﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace QuanLyPhongCongChung
{
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        private void login()
        {            
            if (validateInput())
            {
                string tenDangNhap = txtTenDangNhap.EditValue.ToString();
                string matKhau = txtMatKhau.EditValue.ToString();
                string matKhauDaMaHoa = EncryptionUtility.EncryptString(matKhau, Constants.keyMaHoa);
                using (var db = new DataPhongCongChungDataContext())
                {
                    var nguoiDung = db.NguoiDungs.Where(a => a.UserName == tenDangNhap && a.Password == matKhauDaMaHoa).SingleOrDefault();
                    if (nguoiDung != null)
                    {
                        LoginInfo.nguoiDungInfo = nguoiDung;
                        this.Close();
                    }
                    else
                    {
                        Utilities.ShowMessageError("Tài khoản đăng nhập không đúng");
                        txtTenDangNhap.Focus();
                    }
                }
            }
        }
        private void frmLogin_Load(object sender, EventArgs e)
        {
            //chkGhiNho.Checked = false;
            txtTenDangNhap.Focus();     
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            login();
        }

        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        private bool validateInput()
        {
            if (txtTenDangNhap.EditValue == null)
            {
                Utilities.ShowMessageError("Chưa nhập tên đăng nhập");
                txtTenDangNhap.Focus();
                return false;
            }
            if (txtMatKhau.EditValue == null)
            {
                Utilities.ShowMessageError("Chưa nhập mật khẩu");
                txtMatKhau.Focus();
                return false;
            }
            return true;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmLogin_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
