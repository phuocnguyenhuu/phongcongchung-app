﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongCongChung
{
    public partial class frmUpdateDuongSu : DevExpress.XtraEditors.XtraForm
    {
        public frmUpdateDuongSu()
        {
            InitializeComponent();
        }
        public bool is_insert;
        public string loaiduongsu = "CANHAN";
        public Guid id_DuongSu;
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void frmDuongSuUpdate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Utilities.HotKeyDongForm(e, this);
            }

            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (saveData())
                {
                    Utilities.ShowMessageSuccess("Cập nhật thành công");
                }
                else
                {
                    Utilities.ShowMessageError("Cập nhật thất bại");
                }
            }

        }

        private void frmDuongSuUpdate_Load(object sender, EventArgs e)
        {
            this.lcTinhTrang.Expanded = false;
            loadLoaiGiayToTuyThan();
            if (is_insert)
            {
                if (loaiduongsu == "CANHAN")
                {
                    xtDuongSu.TabPages[1].PageVisible = false;
                    txtTinhTrangSongChet.EditValue = "Sống";
                }
                else
                {
                    xtDuongSu.TabPages[0].PageVisible = false;
                }

            }

            if (!is_insert)
            {
                fillData();
            }

        }


        private void btnLuu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (saveData())
            {
                Utilities.ShowMessageSuccess("Cập nhật thành công");
            }
        }
        private bool saveData()
        {
            try
            {
                if (validateInput())
                {
                    using (var db = new DataPhongCongChungDataContext())
                    {
                        DuongSu ds;
                        if (is_insert)
                        {
                            ds = new DuongSu();
                            ds.Id = Guid.NewGuid();
                            ds.LoaiDoiTuong = loaiduongsu;
                            ds.CreatedBy = LoginInfo.nguoiDungInfo.UserName;
                            ds.CreatedDate = DateTime.Now;
                        }
                        else
                        {
                            ds = db.DuongSus.Where(a => a.Id == id_DuongSu).SingleOrDefault();
                        }

                        if (loaiduongsu == "CANHAN")
                        {
                            //Trường đương sự cá nhân
                            ds.HoTen = (txtHoTen.EditValue != null) ? txtHoTen.EditValue.ToString() : "";

                            ds.NgaySinh = (txtNgaySinh.EditValue != null) ? txtNgaySinh.EditValue.ToString() : "";
                            ds.DienThoai = (txtDienThoai.EditValue != null) ? txtDienThoai.EditValue.ToString() : "";

                            ds.QuocTich = (txtQuocTich.EditValue != null) ? txtQuocTich.EditValue.ToString() : "";

                            ds.NoiCap = (txtNoiCap.EditValue != null) ? txtNoiCap.EditValue.ToString() : "";
                            ds.HoKhauThuongTru = (txtHoKhauThuongTru.EditValue != null) ? txtHoKhauThuongTru.EditValue.ToString() : "";
                            ds.DiaChi = (txtDiaChi.EditValue != null) ? txtDiaChi.EditValue.ToString() : "";
                            ds.MST = (txtMST.EditValue != null) ? txtMST.EditValue.ToString() : "";
                            ds.TinhTrangHonNhan = (txtTinhTrangHonNhan.EditValue != null) ? txtTinhTrangHonNhan.EditValue.ToString() : "";
                            ds.TinhTrangSongChet = (txtTinhTrangSongChet.EditValue != null) ? txtTinhTrangSongChet.EditValue.ToString() : "Sống";
                            if (ds.TinhTrangSongChet == "Chết")
                            {
                                if (txtNgayMat.EditValue != null)
                                {
                                    ds.NgayMat = new DateTime(txtNgayMat.DateTime.Year, txtNgayMat.DateTime.Month, txtNgayMat.DateTime.Day);
                                }
                                else
                                {
                                    ds.NgayMat = null;
                                }
                                ds.NoiMat = (txtNoiMat.EditValue != null) ? txtNoiMat.EditValue.ToString() : null;
                                ds.SoBaoTu = (txtSoBaoTu.EditValue != null) ? txtSoBaoTu.EditValue.ToString() : null;
                                ds.NoiCapGiayChungTu = (txtNoiCapGiayChungTu.EditValue != null) ? txtNoiCapGiayChungTu.EditValue.ToString() : null;
                                if (txtNgayCapGiayChungTu.EditValue != null)
                                {
                                    ds.NgayCapGiayChungTu = Utilities.GetDateTimeFromObjectInput(txtNgayCapGiayChungTu.EditValue);
                                }
                                else
                                {
                                    ds.NgayCapGiayChungTu = null;
                                }
                            }
                            else
                            {
                                ds.NgayMat = null;
                                ds.NoiMat = null;
                                ds.SoBaoTu = null;
                                ds.NoiCapGiayChungTu = null;
                                ds.NgayCapGiayChungTu = null;
                            }
                        }
                        else
                        {
                            //Trường đương sự tổ chức

                            ds.HoTen = (txtTenToChuc.EditValue != null) ? txtTenToChuc.EditValue.ToString() : "";
                            ds.DiaChi = (txtDiaChi_tc.EditValue != null) ? txtDiaChi_tc.EditValue.ToString() : "";
                            ds.DienThoai = (txtDienThoai_tc.EditValue != null) ? txtDienThoai_tc.EditValue.ToString() : "";
                            ds.MST = (txtMST_tc.EditValue != null) ? txtMST_tc.EditValue.ToString() : "";
                            ds.SoGiayPhepKinhDoanh = (txtGiayPhepKinhDoanh.EditValue != null) ? txtGiayPhepKinhDoanh.EditValue.ToString() : "";
                            ds.HoTenNguoiDaiDien = (txtHoTenNguoiDaiDien.EditValue != null) ? txtHoTenNguoiDaiDien.EditValue.ToString() : "";
                            ds.XungHo = (txtXungHo.EditValue != null) ? txtXungHo.EditValue.ToString() : "";
                            ds.ChucVu = (txtChucVu.EditValue != null) ? txtChucVu.EditValue.ToString() : "";
                        }
                        if (txtNgayCap.EditValue != null)
                        {
                            ds.NgayCap = Utilities.GetDateTimeFromObjectInput(txtNgayCap.EditValue);
                        }
                        else
                        {
                            ds.NgayCap = null;
                        }

                        ds.GioiTinh = (txtGioiTinh.EditValue != null) ? txtGioiTinh.EditValue.ToString() : "";
                        ds.LoaiGiayToTuyThan = (txtLoaiGiayToTuyThan.EditValue != null) ? txtLoaiGiayToTuyThan.EditValue.ToString() : "";
                        ds.SoGiayToTuyThan = (txtSoGiayToTuyThan.EditValue != null) ? txtSoGiayToTuyThan.EditValue.ToString() : "";
                        ds.UpdatedBy = LoginInfo.nguoiDungInfo.UserName;
                        ds.UpdatedDate = DateTime.Now;
                        ds.IsDeleted = false;
                        if (is_insert)
                        {
                            db.DuongSus.InsertOnSubmit(ds);
                        }
                        db.SubmitChanges();
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                Utilities.ShowMessageError("Cập nhật thất bại");
                return false;
            }

            return false;
        }
        private bool validateInput()
        {
            if (loaiduongsu == "CANHAN")
            {
                if (Utilities.IsEmptyData(txtHoTen.EditValue))
                {
                    Utilities.ShowMessageError("Chưa nhập họ tên đương sự");
                    txtHoTen.Focus();
                    return false;
                }
                if (Utilities.IsEmptyData(txtGioiTinh.EditValue))
                {
                    Utilities.ShowMessageError("Chưa chọn giới tính");
                    txtGioiTinh.Focus();
                    return false;
                }
                if (Utilities.IsEmptyData(txtLoaiGiayToTuyThan.EditValue))
                {
                    Utilities.ShowMessageError("Chưa loại giấy tờ tùy thân");
                    txtLoaiGiayToTuyThan.Focus();
                    return false;
                }
                if (Utilities.IsEmptyData(txtSoGiayToTuyThan.EditValue))
                {
                    Utilities.ShowMessageError("Chưa nhập số giấy tờ tùy thân");
                    txtSoGiayToTuyThan.Focus();
                    return false;
                }
            }

            if (loaiduongsu == "TOCHUC")
            {
                if (Utilities.IsEmptyData(txtTenToChuc.EditValue))
                {
                    Utilities.ShowMessageError("Chưa nhập tên tổ chức");
                    txtTenToChuc.Focus();
                    return false;
                }
                if (Utilities.IsEmptyData(txtMST_tc.EditValue))
                {
                    Utilities.ShowMessageError("Chưa nhập mã số thuế");
                    txtMST_tc.Focus();
                    return false;
                }

            }

            return true;
        }

        public void fillData()
        {
            try
            {

                using (var db = new DataPhongCongChungDataContext())
                {
                    var updateDuongSu = db.DuongSus.Where(a => a.Id == id_DuongSu).SingleOrDefault();
                    if (loaiduongsu == "CANHAN")
                    {
                        xtDuongSu.SelectedTabPageIndex = 0;
                        xtDuongSu.TabPages[1].PageVisible = false;
                        txtHoTen.EditValue = updateDuongSu.HoTen;
                        txtGioiTinh.EditValue = updateDuongSu.GioiTinh;
                        txtNgaySinh.EditValue = updateDuongSu.NgaySinh;
                        txtDienThoai.EditValue = updateDuongSu.DienThoai;
                        txtLoaiGiayToTuyThan.EditValue = updateDuongSu.LoaiGiayToTuyThan;
                        txtSoGiayToTuyThan.EditValue = updateDuongSu.SoGiayToTuyThan;
                        txtQuocTich.EditValue = updateDuongSu.QuocTich;
                        txtNgayCap.EditValue = updateDuongSu.NgayCap;
                        txtNoiCap.EditValue = updateDuongSu.NoiCap;
                        txtHoKhauThuongTru.EditValue = updateDuongSu.HoKhauThuongTru;
                        txtDiaChi.EditValue = updateDuongSu.DiaChi;
                        txtMST.EditValue = updateDuongSu.MST;
                        txtTinhTrangHonNhan.EditValue = updateDuongSu.TinhTrangHonNhan;
                        txtTinhTrangSongChet.EditValue = updateDuongSu.TinhTrangSongChet;
                        txtNgayMat.EditValue = updateDuongSu.NgayMat;
                        txtNoiMat.EditValue = updateDuongSu.NoiMat;
                        txtSoBaoTu.EditValue = updateDuongSu.SoBaoTu;
                        txtNoiCapGiayChungTu.EditValue = updateDuongSu.NoiCapGiayChungTu;
                        txtNgayCapGiayChungTu.EditValue = updateDuongSu.NgayCapGiayChungTu;
                    }
                    else
                    {
                        xtDuongSu.TabPages[0].PageVisible = false;
                        xtDuongSu.SelectedTabPageIndex = 1;
                        txtTenToChuc.EditValue = updateDuongSu.HoTen;
                        txtDiaChi_tc.EditValue = updateDuongSu.DiaChi;
                        txtDienThoai_tc.EditValue = updateDuongSu.DienThoai;
                        txtMST_tc.EditValue = updateDuongSu.MST;
                        txtGiayPhepKinhDoanh.EditValue = updateDuongSu.SoGiayPhepKinhDoanh;
                        txtNgayCap_tc.EditValue = updateDuongSu.NgayCap;
                        txtHoTenNguoiDaiDien.EditValue = updateDuongSu.HoTenNguoiDaiDien;
                        txtXungHo.EditValue = updateDuongSu.XungHo;
                        txtChucVu.EditValue = updateDuongSu.ChucVu;
                    }


                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi");
            }
        }

        private void txtTinhTrangSongChet_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (txtTinhTrangSongChet.EditValue.ToString().ToUpper() == "CHẾT")
            {
                this.lcTinhTrang.Expanded = true;
                this.Size = new Size(910, 420);
            }
            else
            {
                this.lcTinhTrang.Expanded = false;
                this.Size = new Size(910, 295);
            }
        }

        private void xtDuongSu_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (xtDuongSu.SelectedTabPageIndex == 1)
            {
                loaiduongsu = "TOCHUC";
                this.Size = new Size(910, 260);
            }
            if (xtDuongSu.SelectedTabPageIndex == 0)
            {
                loaiduongsu = "CANHAN";
                this.Size = new Size(910, 295);
            }
        }

        private void loadLoaiGiayToTuyThan()
        {
            using (var db = new DataPhongCongChungDataContext())
            {
                var loaiGiayTos = db.LoaiGiayToTuyThans.OrderBy(a => a.Loai).ToList();
                txtLoaiGiayToTuyThan.Properties.DataSource = loaiGiayTos;
                txtLoaiGiayToTuyThan.Properties.DisplayMember = "Loai";
                txtLoaiGiayToTuyThan.Properties.ValueMember = "Loai";
            }
        }
    }
}
