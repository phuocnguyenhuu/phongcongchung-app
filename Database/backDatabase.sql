USE [master]
GO
/****** Object:  Database [QuanLyPhongCongChung]    Script Date: 12/30/2017 20:57:25 ******/
CREATE DATABASE [QuanLyPhongCongChung] ON  PRIMARY 
( NAME = N'QuanLyPhongCongChung', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\QuanLyPhongCongChung.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QuanLyPhongCongChung_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\QuanLyPhongCongChung_1.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QuanLyPhongCongChung] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuanLyPhongCongChung].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuanLyPhongCongChung] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET ANSI_NULLS OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET ANSI_PADDING OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET ARITHABORT OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [QuanLyPhongCongChung] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [QuanLyPhongCongChung] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [QuanLyPhongCongChung] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET  DISABLE_BROKER
GO
ALTER DATABASE [QuanLyPhongCongChung] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [QuanLyPhongCongChung] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [QuanLyPhongCongChung] SET  READ_WRITE
GO
ALTER DATABASE [QuanLyPhongCongChung] SET RECOVERY SIMPLE
GO
ALTER DATABASE [QuanLyPhongCongChung] SET  MULTI_USER
GO
ALTER DATABASE [QuanLyPhongCongChung] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [QuanLyPhongCongChung] SET DB_CHAINING OFF
GO
USE [QuanLyPhongCongChung]
GO
/****** Object:  Table [dbo].[NguoiDung]    Script Date: 12/30/2017 20:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NguoiDung](
	[Id] [uniqueidentifier] NOT NULL,
	[HoTen] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDelete] [bit] NULL,
	[IsCongChungVien] [bit] NULL,
	[IsAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_NguoiDung] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'fdd25dbd-0140-4afc-8065-02a67bacb1da', N'Phan Văn Nam', N'hvnen', N'ohSFB8/suy8=', NULL, NULL, NULL, NULL, NULL, 0, 0)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'532f4635-63de-4381-990f-21906574d196', N'apvnam1', N'pvnam10', N'm/bLDK+QPSI=', NULL, NULL, NULL, NULL, NULL, 0, 0)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'e3267622-5818-4680-821e-3acdae309cb2', N'aaaaaaaaaaaaa', N'aaaaaaaaaaaaaaa', N'ohSFB8/suy8=', NULL, NULL, NULL, NULL, NULL, 0, 1)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'5bff4786-bbd5-48ef-be7d-53c45bee5074', N'ad', N'anmin', N'vgQ/6tHqhWg=', NULL, NULL, NULL, NULL, 1, 0, 1)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'4c51aa4c-5d7c-454e-b3e5-5441d6f16ce1', N'admin', N'admin', N'ohSFB8/suy8=', NULL, NULL, NULL, NULL, NULL, 0, 1)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'390f6fa3-3285-41f2-a4e0-55c84855f3ed', N'apa', N'pvnam1', N'ojlFgiSWrn0=', NULL, NULL, NULL, NULL, NULL, 0, 0)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'a322ddb7-04ee-4fda-bdd7-730f937fe588', N'Quản trị viên', N'hvnen2', N'ohSFB8/suy8=', N'System', NULL, NULL, NULL, 0, 1, 0)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'a5b6504f-2d05-40e8-96f1-9737781c755c', N'admin', N'23423', N'hgkgjrfOQTg=', NULL, NULL, NULL, NULL, NULL, 1, 1)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'ddb68cd5-7d69-4ea4-838d-c513b1266ed8', N'Phạm Cường', N'PhamCuong', N'ohSFB8/suy8=', NULL, NULL, NULL, NULL, NULL, 1, 1)
INSERT [dbo].[NguoiDung] ([Id], [HoTen], [UserName], [Password], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsCongChungVien], [IsAdmin]) VALUES (N'0dcce71d-1b03-45d1-8679-ed8a6efabcdf', N'ss', N'sa', N'RP8RB37G9b4=', NULL, NULL, NULL, NULL, NULL, 0, 1)
/****** Object:  Table [dbo].[LoaiHoSo]    Script Date: 12/30/2017 20:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiHoSo](
	[Id] [uniqueidentifier] NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleled] [bit] NOT NULL,
 CONSTRAINT [PK_LoaiHoSo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[LoaiHoSo] ([Id], [Ten], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleled]) VALUES (N'721bbcec-669b-4b94-898c-2774b162986d', N'Nam test 02', NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[LoaiHoSo] ([Id], [Ten], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleled]) VALUES (N'5e90de71-49f9-42bb-b560-32971fc8ed19', N'nam testtt', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[LoaiHoSo] ([Id], [Ten], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleled]) VALUES (N'a868a6d6-e262-4e16-8dc6-74aedd0062a6', N'nam test', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[LoaiHoSo] ([Id], [Ten], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleled]) VALUES (N'8eec0345-1af2-4f68-b147-a73990f7521c', N'Công chứng hợp đồng mua bán bất động sản', NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[LoaiHoSo] ([Id], [Ten], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleled]) VALUES (N'8eec0345-1af2-4f68-b147-a73990f7521d', N'hợp đồng', NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[LoaiHoSo] ([Id], [Ten], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleled]) VALUES (N'bf3203e7-e104-4a01-877a-ca705fe83ce6', N'eee', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[LoaiHoSo] ([Id], [Ten], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleled]) VALUES (N'842c5bdc-0457-4e79-8d5e-f35a1cc52c5e', N'nam test', NULL, NULL, NULL, NULL, 0)
/****** Object:  Table [dbo].[LoaiGiayToTuyThan]    Script Date: 12/30/2017 20:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiGiayToTuyThan](
	[Loai] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_LoaiGiayToTuyThan] PRIMARY KEY CLUSTERED 
(
	[Loai] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[LoaiGiayToTuyThan] ([Loai]) VALUES (N'CMND')
INSERT [dbo].[LoaiGiayToTuyThan] ([Loai]) VALUES (N'Hộ chiếu')
INSERT [dbo].[LoaiGiayToTuyThan] ([Loai]) VALUES (N'Thẻ căn cước')
/****** Object:  Table [dbo].[DuongSu]    Script Date: 12/30/2017 20:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DuongSu](
	[Id] [uniqueidentifier] NOT NULL,
	[LoaiDoiTuong] [nvarchar](50) NOT NULL,
	[HoTen] [nvarchar](200) NOT NULL,
	[GioiTinh] [nvarchar](10) NOT NULL,
	[NgaySinh] [nvarchar](50) NULL,
	[LoaiGiayToTuyThan] [nvarchar](50) NOT NULL,
	[SoGiayToTuyThan] [nvarchar](50) NOT NULL,
	[NgayCap] [datetime] NULL,
	[NoiCap] [nvarchar](100) NULL,
	[MST] [nvarchar](50) NULL,
	[HoKhauThuongTru] [nvarchar](200) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[DienThoai] [nvarchar](200) NULL,
	[QuocTich] [nvarchar](50) NULL,
	[TinhTrangSongChet] [nvarchar](50) NULL,
	[TinhTrangHonNhan] [nvarchar](50) NULL,
	[HoTenNguoiDaiDien] [nvarchar](200) NULL,
	[XungHo] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](50) NULL,
	[SoGiayPhepKinhDoanh] [nvarchar](50) NULL,
	[NgayMat] [datetime] NULL,
	[NoiMat] [nvarchar](50) NULL,
	[SoBaoTu] [nvarchar](50) NULL,
	[NoiCapGiayChungTu] [nvarchar](200) NULL,
	[NgayCapGiayChungTu] [datetime] NULL,
	[GhiChu] [text] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_DuongSu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[DuongSu] ([Id], [LoaiDoiTuong], [HoTen], [GioiTinh], [NgaySinh], [LoaiGiayToTuyThan], [SoGiayToTuyThan], [NgayCap], [NoiCap], [MST], [HoKhauThuongTru], [DiaChi], [DienThoai], [QuocTich], [TinhTrangSongChet], [TinhTrangHonNhan], [HoTenNguoiDaiDien], [XungHo], [ChucVu], [SoGiayPhepKinhDoanh], [NgayMat], [NoiMat], [SoBaoTu], [NoiCapGiayChungTu], [NgayCapGiayChungTu], [GhiChu], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted]) VALUES (N'934223b0-6422-4fa6-b8a2-c424be5d9f7d', N'CANHAN', N'Nguyễn Hữu Phước', N'Nam', N'1991', N'CMND', N'374578788', CAST(0x0000A85900000000 AS DateTime), N'6666', N'666', N'666', N'6666', N'6666', N'6666', N'Chết', N'Độc thân', NULL, NULL, NULL, NULL, NULL, N'rrrrr', NULL, NULL, CAST(0x0000A85500000000 AS DateTime), NULL, N'admin', CAST(0x0000A8590120C798 AS DateTime), N'admin', CAST(0x0000A85901215034 AS DateTime), 0)
INSERT [dbo].[DuongSu] ([Id], [LoaiDoiTuong], [HoTen], [GioiTinh], [NgaySinh], [LoaiGiayToTuyThan], [SoGiayToTuyThan], [NgayCap], [NoiCap], [MST], [HoKhauThuongTru], [DiaChi], [DienThoai], [QuocTich], [TinhTrangSongChet], [TinhTrangHonNhan], [HoTenNguoiDaiDien], [XungHo], [ChucVu], [SoGiayPhepKinhDoanh], [NgayMat], [NoiMat], [SoBaoTu], [NoiCapGiayChungTu], [NgayCapGiayChungTu], [GhiChu], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted]) VALUES (N'13ef33cb-fc1c-4d0a-a89f-cd91ffa36dc5', N'TOCHUC', N'Công ty ABC', N'', NULL, N'', N'', NULL, NULL, N'ádasdsad', NULL, N'ádasd', N'ádasd', NULL, NULL, NULL, N'Nguyễn ', N'', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, N'admin', CAST(0x0000A8590121E7F7 AS DateTime), N'admin', CAST(0x0000A8590121F320 AS DateTime), 0)
/****** Object:  Table [dbo].[ThongTinDonVi]    Script Date: 12/30/2017 20:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThongTinDonVi](
	[Id] [uniqueidentifier] NOT NULL,
	[MaPhongCC] [nvarchar](50) NULL,
	[TenPhongCC] [nvarchar](200) NULL,
	[DienThoai] [nvarchar](200) NULL,
	[DiaChi] [nvarchar](500) NULL,
	[Email] [nvarchar](20) NULL,
	[NguoiDaiDien] [nvarchar](200) NULL,
 CONSTRAINT [PK_ThongTinDonVi] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ThongTinDonVi] ([Id], [MaPhongCC], [TenPhongCC], [DienThoai], [DiaChi], [Email], [NguoiDaiDien]) VALUES (N'85d14d81-bcf4-4117-bb5c-cfa6e60745da', N'PCC-PA', N'Văn Phòng Công Chứng Phương Anh', N'0977941517', N'Cần Thơ', N'', N'')
/****** Object:  StoredProcedure [dbo].[sp_getDanhSachDuongSu]    Script Date: 12/30/2017 20:57:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getDanhSachDuongSu] 
	
AS
BEGIN
	SELECT [Id]
      ,[LoaiDoiTuong]
      ,[HoTen]
      ,[GioiTinh]
      ,[NgaySinh]
      ,[LoaiGiayToTuyThan]
      ,[SoGiayToTuyThan]
      ,[NgayCap]
      ,[NoiCap]
      ,[MST]
      ,[HoKhauThuongTru]
      ,[DiaChi]
      ,[DienThoai]
      ,[QuocTich]
      ,[TinhTrangSongChet]
      ,[TinhTrangHonNhan]
      ,[HoTenNguoiDaiDien]
      ,[XungHo]
      ,[ChucVu]
      ,[SoGiayPhepKinhDoanh]
      ,[NgayMat]
      ,[NoiMat]
      ,[SoBaoTu]
      ,[NoiCapGiayChungTu]
      ,[NgayCapGiayChungTu]
      ,[GhiChu]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsDeleted]
	FROM [DuongSu]
	where IsDeleted = 0
	order by [DuongSu].CreatedDate desc
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getThongTinDonVi]    Script Date: 12/30/2017 20:57:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getThongTinDonVi]
	
AS
BEGIN
	select * from 
	dbo.ThongTinDonVi
	where Id = '85d14d81-bcf4-4117-bb5c-cfa6e60745da'
END
GO
/****** Object:  Table [dbo].[HoSo]    Script Date: 12/30/2017 20:57:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoSo](
	[Id] [uniqueidentifier] NOT NULL,
	[LoaiHoSoId] [uniqueidentifier] NOT NULL,
	[TenHopDong] [nvarchar](500) NOT NULL,
	[SoCongChung] [nvarchar](100) NOT NULL,
	[NgayLap] [datetime] NOT NULL,
	[HoTenCongChungVien] [nvarchar](50) NULL,
	[NoiDungHoSo] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[ThongTinTaiSan] [nvarchar](500) NULL,
	[GiaCongChung] [decimal](18, 0) NULL,
	[FileDinhKem] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_HoSo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DuongSuCongChungHoSo]    Script Date: 12/30/2017 20:57:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DuongSuCongChungHoSo](
	[Id] [uniqueidentifier] NOT NULL,
	[DuongSuId] [uniqueidentifier] NULL,
	[HoSoId] [uniqueidentifier] NULL,
	[BenCongChung] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_DuongSuCongChungHoSo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_getListDuongSuCongChungHoSo]    Script Date: 12/30/2017 20:57:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getListDuongSuCongChungHoSo]
	@hoSoId uniqueidentifier
AS
BEGIN
	select 
	BenCongChung,
	HoTen,
	DuongSu.Id as DuongSuId,
	NgaySinh,
	LoaiGiayToTuyThan,
	SoGiayToTuyThan,
	DiaChi
	from dbo.DuongSuCongChungHoSo join DuongSu on dbo.DuongSuCongChungHoSo.DuongSuId=DuongSu.Id 
	where HoSoId=@hoSoId
	and IsDelete=0
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getDanhSachHoSo]    Script Date: 12/30/2017 20:57:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getDanhSachHoSo] 
	@duongSuId uniqueidentifier
AS
BEGIN
	if(@duongSuId='00000000-0000-0000-0000-000000000000')
	begin
		select 
		HoSo.[Id]
		  ,[TenHopDong]
		  ,[SoCongChung]
		  ,[NgayLap]
		  ,[HoTenCongChungVien]
		  ,[NoiDungHoSo]
		  ,[LoaiHoSoId]
		  ,[ThongTinTaiSan]
		  ,[GiaCongChung]
		  ,[FileDinhKem]
		  ,HoSo.[CreatedBy]
		  ,HoSo.[CreatedDate]
		  ,HoSo.[UpdatedBy]
		  ,HoSo.[UpdatedDate]
		  ,HoSo.[IsDeleted]
		  ,GhiChu
		  ,LoaiHoSo.Ten as TenLoaiHoSo
		from 
		
		HoSo join LoaiHoSo on HoSo.LoaiHoSoId = LoaiHoSo.Id
		where IsDeleted=0 order by Hoso.CreatedDate desc
	end else begin
			select 
		HoSo.[Id]
		  ,[TenHopDong]
		  ,[SoCongChung]
		  ,[NgayLap]
		  ,[HoTenCongChungVien]
		  ,[NoiDungHoSo]
		  ,[LoaiHoSoId]
		  ,[ThongTinTaiSan]
		  ,[GiaCongChung]
		  ,[FileDinhKem]
		  ,HoSo.[CreatedBy]
		  ,HoSo.[CreatedDate]
		  ,HoSo.[UpdatedBy]
		  ,HoSo.[UpdatedDate]
		  ,HoSo.[IsDeleted]
		  ,LoaiHoSo.Ten as TenLoaiHoSo
		from 		
		HoSo join LoaiHoSo on HoSo.LoaiHoSoId = LoaiHoSo.Id
		where IsDeleted=0 
		and HoSo.Id in (
			select DuongSuCongChungHoSo.HoSoId from
			dbo.DuongSuCongChungHoSo
			where DuongSuCongChungHoSo.DuongSuId=@duongSuId
		)
		order by Hoso.CreatedDate desc
	end
	
END
GO
/****** Object:  Default [DF_NguoiDung_CreatedDate]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[NguoiDung] ADD  CONSTRAINT [DF_NguoiDung_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_NguoiDung_UpdatedDate]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[NguoiDung] ADD  CONSTRAINT [DF_NguoiDung_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_NguoiDung_IsAdmin]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[NguoiDung] ADD  CONSTRAINT [DF_NguoiDung_IsAdmin]  DEFAULT ((0)) FOR [IsAdmin]
GO
/****** Object:  Default [DF_LoaiHoSo_CreatedBy]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[LoaiHoSo] ADD  CONSTRAINT [DF_LoaiHoSo_CreatedBy]  DEFAULT (N'System') FOR [CreatedBy]
GO
/****** Object:  Default [DF_LoaiHoSo_CreatedDate]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[LoaiHoSo] ADD  CONSTRAINT [DF_LoaiHoSo_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_LoaiHoSo_UpdatedBy]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[LoaiHoSo] ADD  CONSTRAINT [DF_LoaiHoSo_UpdatedBy]  DEFAULT (N'System') FOR [UpdatedBy]
GO
/****** Object:  Default [DF_LoaiHoSo_UpdatedDate]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[LoaiHoSo] ADD  CONSTRAINT [DF_LoaiHoSo_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_LoaiHoSo_IsDeleled]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[LoaiHoSo] ADD  CONSTRAINT [DF_LoaiHoSo_IsDeleled]  DEFAULT ((0)) FOR [IsDeleled]
GO
/****** Object:  Default [DF_DuongSu_Id]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[DuongSu] ADD  CONSTRAINT [DF_DuongSu_Id]  DEFAULT (newid()) FOR [Id]
GO
/****** Object:  Default [DF_DuongSu_LoaiDoiTuong]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[DuongSu] ADD  CONSTRAINT [DF_DuongSu_LoaiDoiTuong]  DEFAULT (N'CANHAN') FOR [LoaiDoiTuong]
GO
/****** Object:  Default [DF_DuongSu_QuocTich]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[DuongSu] ADD  CONSTRAINT [DF_DuongSu_QuocTich]  DEFAULT (N'Việt Nam') FOR [QuocTich]
GO
/****** Object:  Default [DF_DuongSu_TinhTrangSongChet]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[DuongSu] ADD  CONSTRAINT [DF_DuongSu_TinhTrangSongChet]  DEFAULT (N'Sống') FOR [TinhTrangSongChet]
GO
/****** Object:  Default [DF_DuongSu_TinhTrangHonNhan]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[DuongSu] ADD  CONSTRAINT [DF_DuongSu_TinhTrangHonNhan]  DEFAULT (N'Độc Thân') FOR [TinhTrangHonNhan]
GO
/****** Object:  Default [DF_DuongSu_isDeleted]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[DuongSu] ADD  CONSTRAINT [DF_DuongSu_isDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_ThongTinDonVi_Id]    Script Date: 12/30/2017 20:57:27 ******/
ALTER TABLE [dbo].[ThongTinDonVi] ADD  CONSTRAINT [DF_ThongTinDonVi_Id]  DEFAULT (newid()) FOR [Id]
GO
/****** Object:  Default [DF_HoSo_IsDeleted]    Script Date: 12/30/2017 20:57:35 ******/
ALTER TABLE [dbo].[HoSo] ADD  CONSTRAINT [DF_HoSo_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  ForeignKey [FK_HoSo_LoaiHoSo]    Script Date: 12/30/2017 20:57:35 ******/
ALTER TABLE [dbo].[HoSo]  WITH CHECK ADD  CONSTRAINT [FK_HoSo_LoaiHoSo] FOREIGN KEY([LoaiHoSoId])
REFERENCES [dbo].[LoaiHoSo] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HoSo] CHECK CONSTRAINT [FK_HoSo_LoaiHoSo]
GO
/****** Object:  ForeignKey [FK_DuongSuCongChungHoSo_DuongSu]    Script Date: 12/30/2017 20:57:35 ******/
ALTER TABLE [dbo].[DuongSuCongChungHoSo]  WITH CHECK ADD  CONSTRAINT [FK_DuongSuCongChungHoSo_DuongSu] FOREIGN KEY([DuongSuId])
REFERENCES [dbo].[DuongSu] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DuongSuCongChungHoSo] CHECK CONSTRAINT [FK_DuongSuCongChungHoSo_DuongSu]
GO
/****** Object:  ForeignKey [FK_DuongSuCongChungHoSo_HoSo]    Script Date: 12/30/2017 20:57:35 ******/
ALTER TABLE [dbo].[DuongSuCongChungHoSo]  WITH CHECK ADD  CONSTRAINT [FK_DuongSuCongChungHoSo_HoSo] FOREIGN KEY([HoSoId])
REFERENCES [dbo].[HoSo] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DuongSuCongChungHoSo] CHECK CONSTRAINT [FK_DuongSuCongChungHoSo_HoSo]
GO
